import path from "path"
import express from "express"
// import a library which allows us to throw errors in async route handlers
import "express-async-errors"
import { json } from "body-parser"
import cookieSession from "cookie-session"
import swStats from "swagger-stats"
import fileupload from "express-fileupload"
import helmet from "helmet"
import hpp from "hpp"
import cors from "cors"

// import the express route handlers
import { getUserRouter } from "./routes/user"
import { showUserRouter } from './routes/show'
import { currentUserRouter } from "./routes/current-user"
import { signinRouter } from "./routes/signin"
import { signoutRouter } from "./routes/signout"
import { signupRouter } from "./routes/signup"
import { healthzRouter } from "./routes/healthz"
import { deleteOrderRouter } from './routes/delete'


import { updateUserRouter } from './routes/update'

// import the express error middleware
import { errorHandler, NotFoundError } from "@relenv/common"

// had to add this because i ran out of listeners on my mac
require("events").EventEmitter.defaultMaxListeners = 25

// setup the express app which we will export
const app = express()
app.set("trust proxy", true) // ingress service acts as a proxy so add this so express works

app.use(json())
// setup the cookies securing when not in test
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [ process.env.COOKIE_KEY! ],
    signed: false,
    secure: false, //process.env.NODE_ENV !== "test",
  })
)
// add the swagger and prometheus metrics
// /api/v1/users/ui  for the swagger stats ui
// /api/v1/users/metrics for the prometheus endpoint
app.use(
  swStats.getMiddleware({
    uriPath: "/api/v1/users",
  })
)

// File uploading
app.use(fileupload());

// Set security headers
app.use(helmet());

// Prevent http param pollution
app.use(hpp());

// Enable CORS
app.use(cors());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// routes for the service
app.use(healthzRouter)
app.use(currentUserRouter)
app.use(getUserRouter)
app.use(signinRouter)
app.use(signoutRouter)
app.use(signupRouter)
app.use(deleteOrderRouter)
app.use(updateUserRouter)

app.use(showUserRouter)


// handle routes we were not expectinng
app.all("*", async () => {
  throw new NotFoundError()
})
// handle any thrown errors
app.use(errorHandler)

export { app }

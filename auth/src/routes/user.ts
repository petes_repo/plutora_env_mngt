import express, { Request, Response } from "express"
import { User } from "../models/user"
import { NotFoundError, currentUser} from "@relenv/common"

const router = express.Router()

router.get("/api/v1/users/:id", currentUser, async (req: Request, res: Response) => {
  // get the id off the parameter
  let user
  try {
    user = await User.findById(req.params.id)
    // if no record found throw an error
    if (!user) {
      throw new NotFoundError()
    }
  } catch (err) {
    console.log("user-get error")
    console.log(err)
    throw new NotFoundError()
  }
  // return the ticket to the requestor
  res.status(200).send(user)
})

export { router as getUserRouter }

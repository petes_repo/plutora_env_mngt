import express, { NextFunction, Request, Response } from "express"
const path = require('path');

import {
  requireAuth,
  NotFoundError,
  currentUser,
  BadRequestError
} from "@relenv/common"

import { User } from "../models/user"
import { UploadedFile } from "express-fileupload"

const router = express.Router()

router.put(
  "/api/v1/users/:id/photo",
  currentUser,
  requireAuth,
  async (req: Request, res: Response, next: NextFunction) => {
    let user
    // get the current user
    try {
      user = await User.findById(req.params.id)
    } catch(err) {
      throw new BadRequestError("Failed to retrieve user")
    }

    // check that we found the record
    if (!user) {
      throw new NotFoundError()
    }

    if (req.currentUser!.id !== user.id) {
      throw new BadRequestError("Can only upload photos on your profile")
    }

    if (!req.files) {
      throw new BadRequestError("Please upload an image file")
    }

    const file = req.files.file as UploadedFile

    // Make sure the image is a photo
    if (!file.mimetype.startsWith('image')) {
      throw new BadRequestError("Please upload an image file")
    }

    // Check filesize
    if ((file.size) > parseInt(process.env.MAX_FILE_UPLOAD!)) {
      throw new BadRequestError("Please upload an image file")
    }

    // Create custom filename
    file.name = `photo_${user._id}${path.parse(file.name).ext}`

    try {
      file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if (err) {
          throw new BadRequestError("Please upload an image file")
        }
    
        await User.findByIdAndUpdate(req.params.id, { photo: file.name });
    
        res.status(200).json({
          success: true,
          data: file.name
        });
      })
    } catch (err) {
      throw new BadRequestError("Failed to save the image")
    }
  },
)

export { router as updateUserRouter }

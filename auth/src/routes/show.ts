import express, { Request, Response } from "express"
import { User } from "../models/user"
import { currentUser, authorize } from "@relenv/common"

const router = express.Router()

router.get("/api/v1/users",  currentUser, authorize('admin'), async (req: Request, res: Response) => {
  const users = await User.find({  })
  res.send(users)
})

export { router as showUserRouter }
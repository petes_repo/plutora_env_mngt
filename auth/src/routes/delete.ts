import express, { Request, Response } from "express"
import {
  requireAuth,
  BadRequestError,
  currentUser,
} from "@relenv/common"
import { User, UserDoc } from "../models/user"

const router = express.Router()
// delete order route
router.delete(
  "/api/v1/users/:userId",
  currentUser,
  requireAuth,
  async (req: Request, res: Response) => {
    try {
      const user = await User.findById(req.params.userId)

      if (!user) {
        throw new BadRequestError("User not found")
      }

      user.remove()

      res.status(200).json(user)
    } catch (err) {
      throw new BadRequestError("Something went wrong")
    }
  },
)

export { router as deleteOrderRouter }
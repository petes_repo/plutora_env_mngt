import express, { Request, Response } from "express"
import { body } from "express-validator"

import {
  requireAuth,
  validateRequest,
  NotFoundError,
  currentUser,
  BadRequestError
} from "@relenv/common"

import { User } from "../models/user"

const router = express.Router()

router.put(
  "/api/v1/users/:id",
  currentUser,
  requireAuth,
  [
    body("email").isEmail().withMessage("Email must be valid"),
    body("password")
      .trim()
      .custom((value) => {
        if (value.length > 0) {
          if (value.length < 6) {
            return Promise.reject('Password must be at least 6 characters');
          }
        }
      })
      .withMessage("Password must be 6 characters or greater"),
    body("role")
      .trim()
      .custom((value) => {
        if (value !== 'admin' && value !== 'user') {
          return Promise.reject('Role must be user or admin');
        }
      }),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    let user
    // find the user in the database
    try {
      user = await User.findById(req.params.id)
    } catch(err) {
      console.log("Auth-update-find")
      console.log(err)
    }

    // check that we found the record
    if (!user) {
      throw new NotFoundError()
    }
    try {
      // use set to change the fields
      user.set({...req.body})
      // save back to database
      await user.save()
    } catch(err) {
      console.log("Auth-update-save")
      console.log(err)     
      throw new BadRequestError(`Error saving to database: ${err}`)
    }
    // respond to requestor
    res.status(200).send(user)
  },
)

export { router as updateUserRouter }

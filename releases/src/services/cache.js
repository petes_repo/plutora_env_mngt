const mongoose = require("mongoose")
const redis = require("redis")
const util = require("util")

const client = redis.createClient(process.env.redisUrl)
client.hget = util.promisify(client.hget)
const exec = mongoose.Query.prototype.exec

// this function is called if a query needs to bbe cached
mongoose.Query.prototype.cache = function (options = {}) {
  this.useCache = true
  this.hashKey = JSON.stringify(options.key || "")
  // need to return this to make it chainable
  return this
}

mongoose.Query.prototype.exec = async function () {
  // check to see whether we need to use cache for this querry
  if (!this.useCache) {
    //  cache not used so returnn query
    return exec.apply(this, arguments)
  }

  const key = JSON.stringify(
    // use object assign to copy the query and collection to new object
    // then stringify it to get unique key for each query
    Object.assign({}, this.getQuery(), {
      collection: this.mongooseCollection.name,
    })
  )

  // See if we have a value for 'key' in redis
  const cacheValue = await client.hget(this.hashKey, key)

  // If we do, return that
  // we need to make sure we are returning a model document type
  if (cacheValue) {
    const doc = JSON.parse(cacheValue)

    return Array.isArray(doc)
      ? doc.map((d) => new this.model(d))
      : new this.model(doc)
  }

  // Otherwise, issue the query and store the result in redis
  const result = await exec.apply(this, arguments)

  client.hset(this.hashKey, key, JSON.stringify(result), "EX", 10)

  return result
}

module.exports = {
  clearHash(hashKey) {
    client.del(JSON.stringify(hashKey))
  },
}

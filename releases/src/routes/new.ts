import express, { Request, Response } from "express"
import { body } from "express-validator"

import { requireAuth, validateRequest } from "@plutora_env/common"
import { Release } from "../models/release"
// import event publishers
import { ReleaseCreatedPublisher } from "../events/publishers/release-created-publisher"
// import the nats connection object
import { natsWrapper } from "../nats-wrapper"

const router = express.Router()

router.post(
  "/api/releases",
  requireAuth,
  [
    body("title").not().isEmpty().withMessage("Title is required"),
    body("portfolio").not().isEmpty().withMessage("Portfolio is required"),
    body("description").not().isEmpty().withMessage("Portfolio is required"),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const {
      title,
      portfolio,
      description,
      release_manager,
      type,
      targetdate,
      status,
      risk,
    } = req.body

    // use the build method to create record
    // requireAuth has already checked currentUser
    const release = Release.build({
      title,
      portfolio,
      description,
      release_manager,
      type,
      targetdate,
      status,
      risk,
    })

    try {
      await release.save()
      new ReleaseCreatedPublisher(natsWrapper.client).publish({
        id: release.id,
        title: release.title,
        portfolio: release.portfolio,
        description: release.description,
        release_manager: release.release_manager,
        type: release.type,
        targetdate: release.targetdate,
        status: release.status,
        risk: release.risk,
      })
    } catch (err) {
      console.log(err.message)
      throw new Error("new.ts: Error saving Release")
    }
    res.status(201).send(release)
  }
)

export { router as createReleaseRouter }

import express, { Request, Response } from "express"
import { Release } from "../models/release"
import { NotFoundError } from "@plutora_env/common"

const router = express.Router()

router.get("/api/tickets/:id", async (req: Request, res: Response) => {
  // get the id off the parameter
  const release = await Release.findById(req.params.id)
  // if no record found throw an error
  if (!release) {
    throw new NotFoundError()
  }
  // return the ticket to the requestor
  res.status(200).send(release)
})

export { router as showReleaseRouter }

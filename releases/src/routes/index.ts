import express, { Request, Response } from "express"
import { Release } from "../models/release"

const router = express.Router()

router.get("/api/releases", async (req: Request, res: Response) => {
  const releases = await Release.find({})
  res.send(releases)
})

export { router as indexReleaseRouter }

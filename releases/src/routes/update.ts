import express, { Request, Response } from "express"
import { body } from "express-validator"
import { ReleaseUpdatedPublisher } from "../events/publishers/release-updated-publisher"
import { natsWrapper } from "../nats-wrapper"
import {
  requireAuth,
  validateRequest,
  NotFoundError,
  NotAuthorisedError,
  BadRequestError,
} from "@plutora_env/common"
import { Release } from "../models/release"

const router = express.Router()

router.put(
  "/api/releases/:id",
  requireAuth,
  [
    body("title").not().isEmpty().withMessage("Title is required"),
    body("portfolio").not().isEmpty().withMessage("Portfolio is required"),
    body("description").not().isEmpty().withMessage("Portfolio is required"),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const release = await Release.findById(req.params.id)
    // check that we found the record
    if (!release) {
      throw new NotFoundError()
    }
    // // check to see whether the ticket has been reserved
    // if (ticket.orderId) {
    //   throw new BadRequestError("Cannot edit a reserved ticket")
    // }
    // // check that the record belongs to this user
    // if (ticket.userId !== req.currentUser!.id) {
    //   throw new NotAuthorisedError()
    // }
    const {
      title,
      portfolio,
      description,
      release_manager,
      type,
      targetdate,
      status,
      risk,
    } = req.body

    // use set to change the fields
    release.set({
      title,
      portfolio,
      description,
      release_manager,
      type,
      targetdate,
      status,
      risk,
    })
    // save back to database
    await release.save()
    // emit event with the update details
    new ReleaseUpdatedPublisher(natsWrapper.client).publish({
      id: release.id,
      title: release.title,
      portfolio: release.portfolio,
      description: release.description,
      release_manager: release.release_manager,
      type: release.type,
      targetdate: release.targetdate,
      status: release.status,
      risk: release.risk,
    })
    // respond to requestor
    res.status(200).send(release)
  }
)

export { router as updateReleaseRouter }

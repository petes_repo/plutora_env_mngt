// express middleware that is included in routes that cache data

const { clearHash } = require("../services/cache")
import { Request, Response, NextFunction } from "express"

// tells typescript we have added a variable to the Express Request object type
declare global {
  namespace Express {
    interface Request {
      key?: string
    }
  }
}

module.exports = async (req: Request, res: Response, next: NextFunction) => {
  // wait for functio to complete first
  await next()

  clearHash(req.key)
}

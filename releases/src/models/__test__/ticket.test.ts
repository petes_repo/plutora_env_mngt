import { Release } from "../release"

it("implements optimistic concurrency control", async (done) => {
  // Create an instance of a ticket
  const release = Release.build({
    title: "concert",
    price: 5,
    userId: "123",
  })

  // Save the ticket to the database
  await release.save()

  // fetch the ticket twice
  const firstInstance = await Release.findById(release.id)
  const secondInstance = await Release.findById(release.id)

  // make two separate changes to the tickets we fetched
  firstInstance!.set({ price: 10 })
  secondInstance!.set({ price: 15 })

  // save the first fetched ticket
  await firstInstance!.save()

  // save the second fetched ticket and expect an error
  try {
    await secondInstance!.save()
  } catch (err) {
    return done()
  }

  throw new Error("Should not reach this point")
})

it("increments the version number on multiple saves", async () => {
  const release = Release.build({
    title: "concert",
    price: 20,
    userId: "123",
  })

  await release.save()
  expect(release.version).toEqual(0)
  await release.save()
  expect(release.version).toEqual(1)
  await release.save()
  expect(release.version).toEqual(2)
})

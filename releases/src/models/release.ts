import mongoose from "mongoose"
// attributes used to create the Ticket
interface ReleaseAttrs {
  title: string
  portfolio: string
  description: string
  release_manager: string
  type: string
  targetdate: string
  status: string
  risk: string
}

// Describes what is stored
interface ReleaseDoc extends mongoose.Document {
  title: string
  portfolio: string
  description: string
  release_manager: string
  type: string
  targetdate: string
  status: string
  risk: string
}

// adds the build function to the model
interface ReleaseModel extends mongoose.Model<ReleaseDoc> {
  build(attrs: ReleaseAttrs): ReleaseDoc
}

//define the document and what json it returns
const releaseSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    portfolio: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    release_manager: {
      type: String,
    },
    type: {
      type: String,
    },
    targetdate: {
      type: Date,
    },
    status: {
      type: String,
    },
    risk: {
      type: String,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

// built so typescript knows about the attributes
releaseSchema.statics.build = (attrs: ReleaseAttrs) => {
  return new Release(attrs)
}

const Release = mongoose.model<ReleaseDoc, ReleaseModel>(
  "Release",
  releaseSchema
)

export { Release }

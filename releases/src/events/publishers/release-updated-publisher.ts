import { Publisher, Subjects, ReleaseUpdatedEvent } from "@plutora_env/common"

// extend the base Publisher class and associate with the TicketUpdatedEvent definition
export class ReleaseUpdatedPublisher extends Publisher<ReleaseUpdatedEvent> {
  readonly subject = Subjects.ReleaseUpdated
}

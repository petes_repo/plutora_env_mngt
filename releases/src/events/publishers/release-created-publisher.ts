import { Publisher, Subjects, ReleaseCreatedEvent } from "@plutora_env/common"

// extend the base Publisher class and associate with the ReleaseCreatedEvent definition
export class ReleaseCreatedPublisher extends Publisher<ReleaseCreatedEvent> {
  readonly subject = Subjects.ReleaseCreated
}

import { Message } from "node-nats-streaming"
import { Listener, ProjectCreatedEvent, Subjects } from "@plutora_env/common"
import { queueGroupName } from "./queue-group-name"
import { Release } from "../../models/release"
import { ReleaseUpdatedPublisher } from "../publishers/release-updated-publisher"

// extend the base listener class and tie to the subject to data binding
export class releaseCreatedListener extends Listener<ProjectCreatedEvent> {
  // set read only field recording the subject to publish to
  readonly subject = Subjects.ProjectCreated
  // set the queue group this service uses in NATS
  queueGroupName = queueGroupName

  // define the function that the listen event invokes
  async onMessage(data: ProjectCreatedEvent["data"], msg: Message) {
    // Find the release that the project is against
    const release = await Release.findById(data.release.id)
    const project = await Project.findById(data.project.id)
    // If no release, throw error
    if (!release) {
      if (!project) {
        // nothing to update
        msg.ack()
      } else {
        // project no loger associated with release delete it
        project.delete
      }
    } else {
    }

    // Mark the ticket as being reserved by setting its orderId property
    release.set({ orderId: data.id })

    // Save the ticket and publish this event to NATS
    await ticket.save()
    // need to publish an update to make sure the version no. is cascaded
    await new TicketUpdatedPublisher(this.client).publish({
      id: ticket.id,
      price: ticket.price,
      title: ticket.title,
      userId: ticket.userId,
      orderId: ticket.orderId,
      version: ticket.version,
    })

    // tell NATS we have successfully processed the event
    msg.ack()
  }
}

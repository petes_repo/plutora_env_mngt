module.exports = {
  JWT_KEY: process.env.JWT_KEY,
  NEO4J_URI: process.env.MONGO_URI,
  NATS_CLIENT_ID: process.env.NATS_CLIENT_ID,
  NATS_URL: process.env.NATS_URL,
  NATS_CLUSTER_ID: process.env.NATS_CLUSTER_ID,
  GITLAB_URI: process.env.GITLAB_URI,
  GITLAB_TOKEN: process.env.GITLAB_TOKEN
}

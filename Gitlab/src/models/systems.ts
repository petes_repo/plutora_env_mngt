import mongoose from "mongoose"
// an interface that describes the properties that are required to create a new user
interface SystemAttrs {
  pl_id: string
  status: string
  name: string
  orgid: string
  description: string
}

// an interface that describes the build function for typescript
interface SystemModel extends mongoose.Model<SystemDoc> {
  build(attrs: SystemAttrs): SystemDoc
}

// an interface that describes the props that a user document has
export interface SystemDoc extends mongoose.Document {
  pl_id: string
  status: string
  name: string
  orgid: string
  description: string
}

// defines the mongoDB structure
// change the default json fields returned by redefining toJSON
const systemSchema = new mongoose.Schema(
  {
    pl_id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    orgid: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

// done this way to allow typescript to understand the attributes
// this is because you use User.build() instead of new User
systemSchema.statics.build = (attrs: SystemAttrs) => {
  return new System(attrs)
}

// setup the model
const System = mongoose.model<SystemDoc, SystemModel>("System", systemSchema)

export { System }

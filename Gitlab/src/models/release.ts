import mongoose from "mongoose"
// an interface that describes the properties that are required to create a new user
interface ReleaseAttrs {
  pl_id: string
  identifier: string
  name: string
  implementationDate: string
  displayColor: string
  plutoraReleaseType: string
  releaseProjectType: string
  releaseStatusType: string
}

// an interface that describes the build function for typescript
interface ReleaseModel extends mongoose.Model<ReleaseDoc> {
  build(attrs: ReleaseAttrs): ReleaseDoc
  openReleases(): ReleaseDoc[]
}

// an interface that describes the props that a user document has
export interface ReleaseDoc extends mongoose.Document {
  pl_id: string
  identifier: string
  name: string
  implementationDate: string
  displayColor: string
  plutoraReleaseType: string
  releaseProjectType: string
  releaseStatusType: string
}

// defines the mongoDB structure
// change the default json fields returned by redefining toJSON
const releaseSchema = new mongoose.Schema(
  {
    pl_id: {
      type: String,
      required: true,
    },
    identifier: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    summary: {
      type: String,
    },
    implementationDate: {
      type: String,
      required: true,
    },
    displayColor: {
      type: String,
      required: true,
    },
    plutoraRelease: {
      type: String,
      required: true,
    },
    releaseProjectType: {
      type: String,
      required: true,
    },
    releaseStatus: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

// done this way to allow typescript to understand the attributes
// this is because you use User.build() instead of new User
releaseSchema.statics.build = (attrs: ReleaseAttrs) => {
  return new Release(attrs)
}

// add function to the document instead of the Model
releaseSchema.statics.openReleases = async function () {
  // this === the ticket document that we just called 'isReserved' on
  const openReleases = await Release.find({
    status: {
      $in: ["Approved", "In Process"],
    },
  })

  return openReleases
}

// setup the model
const Release = mongoose.model<ReleaseDoc, ReleaseModel>(
  "Release",
  releaseSchema
)

export { Release }

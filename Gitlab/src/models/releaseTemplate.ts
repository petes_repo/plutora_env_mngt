import mongoose from "mongoose"
// an interface that describes the properties that are required to create a new user
interface ReleaseTemplateAttrs {
  pl_id: string
  name: string
}

// an interface that describes the build function for typescript
interface ReleaseTemplateModel extends mongoose.Model<ReleaseTemplateDoc> {
  build(attrs: ReleaseTemplateAttrs): ReleaseTemplateDoc
}

// an interface that describes the props that a user document has
export interface ReleaseTemplateDoc extends mongoose.Document {
  pl_id: string
  name: string
}

// defines the mongoDB structure
// change the default json fields returned by redefining toJSON
const releaseTemplateSchema = new mongoose.Schema(
  {
    pl_id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

// done this way to allow typescript to understand the attributes
// this is because you use User.build() instead of new User
releaseTemplateSchema.statics.build = (attrs: ReleaseTemplateAttrs) => {
  return new ReleaseTemplate(attrs)
}

// setup the model
const ReleaseTemplate = mongoose.model<
  ReleaseTemplateDoc,
  ReleaseTemplateModel
>("ReleaseTemplate", releaseTemplateSchema)

export { ReleaseTemplate }

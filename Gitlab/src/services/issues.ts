import { plutoraWrapper } from "../gitlab-wrapper"
import {
  BadRequestError,
  NotFoundError,
  validateRequest,
  requireAuth,
  currentUser,
} from "@relenv/common"
import { getReleaseByIdentifier } from "./projects"
////////////////////////////////////////////////////////
//////// Plutora API data loading functions ////////

// @desc      get create new change in plutora
// @param     { }
// @access    Public
export const createNewChange = async (data: any) => {
  let newChangeData = {}
  try {
    newChangeData = mapNewChange(data)
    let newChange = await plutoraWrapper.plutoraRequest(
      "POST",
      "changes",
      JSON.stringify(newChangeData)
    )
    return newChange
  } catch (err) {
    console.error(
      `<<< Issue posting change with (${newChangeData})  with error ${err} >>>`
    )
  }
}
// function to map pull requests to changes
const mapNewChange = async function (data: any) {
  // vet the passed in data
  if (!data.rel_identifier) {
    throw new BadRequestError("Release identifier must be provided")
  }
  if (!data.title) {
    throw new BadRequestError("change title must be provided")
  }
  if (!data.created_at) {
    throw new BadRequestError("change created_at must be provided")
  }
  if (!data.status) {
    throw new BadRequestError("change status must be provided")
  }
  if (!data.description) {
    throw new BadRequestError("change description must be provided")
  }
  // need to retrieve the release information
  const release = await getReleaseByIdentifier(data.rel_identifier)

  return {
    id: null,
    name: data.title,
    changeId: null,
    changePriorityId: plutoraWrapper.changePriorities["Medium"].id,
    changePriority: "Medium",
    changeStatusId: plutoraWrapper.changeStatuses[data.status].id,
    changeStatus: data.status,
    businessValueScore: null, // 80
    raisedById: "2799ca7b-7301-eb11-aa62-e6b22d29743d",
    raisedBy: "Peter Gross",
    raisedDate: data.created_at,
    organizationId: plutoraWrapper.organisations["Plutora Integrations"].id,
    organization: "Plutora Integrations", //
    changeTypeId: plutoraWrapper.changeType["Feature"].id,
    changeType: "Feature",
    changeDeliveryRiskId: plutoraWrapper.deliveryRisks["Low"].id,
    changeDeliveryRisk: "Low",
    expectedDeliveryDate: "2018-04-24T00:00:00",
    changeThemeId: plutoraWrapper.changeThemes["Strategic"].id,
    changeTheme: "Strategic",
    description: data.description,
    descriptionSimple: "",
    lastModifiedDate: null,
    lastModifiedBy: null,
    additionalInformation: [
      {
        id: "217351ef-7e01-eb11-aa62-e6b22d29743d",
        name: "Dependencies",
        type: "Change",
        dataType: "FreeText",
        text: ``,
        time: null,
        date: null,
        number: null,
        decimal: null,
        dateTime: null,
        listItem: null,
        multiListItem: null,
      },
      {
        id: "7cbc8821-910c-eb11-aa62-e6b22d29743d",
        name: "Gitlab_issue_id",
        type: "Change",
        dataType: "FreeText",
        text: data.gitlab.id || "",
        time: null,
        date: null,
        number: null,
        decimal: null,
        dateTime: null,
        listItem: null,
        multiListItem: null,
      },
      {
        id: "7dbc8821-910c-eb11-aa62-e6b22d29743d",
        name: "Gitlab_project_id",
        type: "Change",
        dataType: "FreeText",
        text: data.gitlab.projectId || "",
        time: null,
        date: null,
        number: null,
        decimal: null,
        dateTime: null,
        listItem: null,
        multiListItem: null,
      },
      {
        id: "227351ef-7e01-eb11-aa62-e6b22d29743d",
        name: "Versions",
        type: "Change",
        dataType: "FreeText",
        text: "",
        time: null,
        date: null,
        number: null,
        decimal: null,
        dateTime: null,
        listItem: null,
        multiListItem: null,
      },
    ],
    stakeholders: [
      {
        id: "c6d730a1-5809-eb11-aa62-e6b22d29743d",
        userId: "2799ca7b-7301-eb11-aa62-e6b22d29743d",
        user: "Peter Gross",
        groupId: null,
        group: "",
        stakeholderRoleIds: ["693d1db2-6c01-eb11-aa62-e6b22d29743d"],
        stakeholderRoles: "Release Manager",
        responsible: false,
        accountable: true,
        informed: false,
        consulted: false,
      },
    ],
    systems: [
      {
        systemId: "3a5415b7-d304-eb11-aa62-e6b22d29743d",
        system: "Orders",
        systemRoleType: "Impact",
      },
    ],
    deliveryReleases: [
      {
        releaseId: release.id,
        releaseIdentifier: release.identifier,
        releaseName: release.name,
        enterpriseReleaseIdentifier: null,
        implementationDate: release.implementationDate,
        targetRelease: true,
        actualDeliveryRelease: true,
      },
    ],
    comments: [],
  }
}

// get a change by its ID
export const getChangeByID = async function (changeID: string) {
  let result = ""

  try {
    result = await plutoraWrapper.plutoraRequest("GET", `changes/${changeID}`)
  } catch (err) {
    console.error(
      `<<< Issue getting change with (${changeID})  with error ${err} >>>`
    )
  }

  return result
}
// get a change by its ID
export const getChangeByName = async function (changeName: string) {
  let result = ""

  try {
    result = await plutoraWrapper.plutoraRequest(
      "GET",
      `changes?filter=%60name%60%20equals%20%60${changeName}%60`
    )
  } catch (err) {
    console.error(
      `<<< Issue getting change with (${changeName}) with error ${err} >>>`
    )
  }

  return result
}
// get linked changes
export const getChangesLinkedChanges = async function (changeID: string) {
  try {
    return await plutoraWrapper.plutoraRequest(
      "GET",
      `changes/${changeID}/linkedChanges`
    )
  } catch (err) {
    console.error(
      `<<< Issue getting linked changes (${changeID})  with error ${err} >>>`
    )
  }
}

// update linked changes
export const updateChangesLinkedChanges = async function (
  changeID: string,
  linkedChangesInfo: any
) {
  console.log(`<<< Updating linked changes of Change: (${changeID}) >>>`)
  try {
    return await plutoraWrapper.plutoraRequest(
      "PUT",
      `changes/${changeID}/linkedChanges`,
      JSON.stringify(linkedChangesInfo)
    )
  } catch (err) {
    console.error(
      `<<< Issue updating linked changes (${changeID})  with error ${err} >>>`
    )
  }
}

// update change by its ID
export const updateChange = async function (
  changeID: string,
  updateChangeInfo: any
) {
  console.log(`<<< Updating change (${changeID}) >>>`)
  try {
    await plutoraWrapper.plutoraRequest(
      "PUT",
      `changes/${changeID}`,
      JSON.stringify(updateChangeInfo)
    )
  } catch (err) {
    console.error(
      `<<< Issue updating change (${changeID})  with error ${err} >>>`
    )
  }
}

// get all changes
export const getAllChanges = async function () {
  return await plutoraWrapper.plutoraRequest("GET", "/changes")
}

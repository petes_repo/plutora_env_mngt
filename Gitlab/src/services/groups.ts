import { plutoraWrapper } from "../gitlab-wrapper"
// import error handlers
import {
  BadRequestError,
  NotFoundError,
  validateRequest,
  requireAuth,
  currentUser,
} from "@relenv/common"

////////////////////////////////////////////////////////
//////// Plutora API data loading functions ////////

/**
 * @description create new Release in plutora
 * @param {object} data - The data to create a new release from
 * @param {string} data.identifier - short name of the release
 * @param {string} data.name - long name of the release
 * @param {string} data.summary - description of the release
 * @param {string} data.implementationDate - the release implementation date
 * @param {string} data.plutoraReleaseType - release type default Major
 * @param {string} data.releaseStatusType - status default Draft
 * @param {string} data.gitLabMilestoneId - The Gitlab milestone id
 * @param {string} data.gitLabProjectId - The Gitlab project id
 * @returns {Promise} Returns the release record just created in Plutora
 */
export const createNewRelease = async (data: TypeCreateRelease) => {
  // vet mandatory input data
  if (!data.identifier) {
    throw new BadRequestError("Release identifier must be provided")
  }
  if (!data.name) {
    throw new BadRequestError("Release name must be provided")
  }
  if (!data.summary) {
    throw new BadRequestError("Release summary must be provided")
  }
  if (!data.implementationDate) {
    throw new BadRequestError("Release implementationDate must be provided")
  }

  let newReleaseData = {}
  try {
    // Create the js object to create the new record
    newReleaseData = mapNewRelease(data)
    // call the plutora api to create the new release
    let newRelease = await plutoraWrapper.plutoraGetRequest(
      "releases"
    )
    return newRelease
  } catch (err) {
    console.error(
      `<<< Issue posting change with (${newReleaseData})  with error ${err} >>>`
    )
  }
  // need to publish that a new release record has been created

}

type TypeCreateRelease = {
  identifier: string // short name of the release
  name: string // long name of the release
  summary: string // description of the release
  implementationDate: string // the implementation date
  plutoraReleaseType?: string // default Major
  releaseStatusType?: string // default Draft
  gitLabMilestoneId?: string
  gitLabProjectId?: string
}

// function to map pull requests to changes
const mapNewRelease = async function (
  data: TypeCreateRelease
): Promise<TypeReleaseDetails> {
  // might want to retrieve a template and then alter it

  return {
    id: null,
    identifier: data.identifier,
    name: data.name,
    summary: data.summary,
    releaseTypeId:
      plutoraWrapper.releaseType[data.plutoraReleaseType || "Major"].id,
    releaseType: data.plutoraReleaseType || "Major",
    location: "",
    releaseStatusTypeId:
      plutoraWrapper.releaseStatus[data.releaseStatusType || "Draft"].id,
    releaseStatusType: data.releaseStatusType || "Draft",
    releaseRiskLevelId: plutoraWrapper.releaseRiskLevel["Medium"].id,
    releaseRiskLevel: "Medium",
    implementationDate: data.implementationDate,
    displayColor: "#0000ff",
    organizationId: plutoraWrapper.organisations["Plutora Integrations"].id,
    organization: "Plutora Integrations",
    managerId:plutoraWrapper.users["pete.gross@btinternet.com"].id,
    manager: "Peter Gross",
    parentReleaseId: null,
    parentRelease: null,
    plutoraReleaseType: "Independent",
    releaseProjectType: "IsProject",
    additionalInformation: [
      {
        id: "3c1860f4-910c-eb11-aa62-e6b22d29743d",
        name: "Gitlab_milestone_id",
        type: "Release",
        dataType: "FreeText",
        text: data.gitLabMilestoneId || null,
        time: null,
        date: null,
        number: null,
        decimal: null,
        dateTime: null,
        listItem: null,
        multiListItem: null,
      },
      {
        id: "3d1860f4-910c-eb11-aa62-e6b22d29743d",
        name: "Gitlab_project_id",
        type: "Release",
        dataType: "FreeText",
        text: data.gitLabProjectId || null,
        time: null,
        date: null,
        number: null,
        decimal: null,
        dateTime: null,
        listItem: null,
        multiListItem: null,
      },
    ],
    lastModifiedDate: new Date().toISOString(),
  }
}

// used by the get releases interface
interface TypeRelease {
  id: string
  identifier: string
  name: string
  implementationDate: Date
  displayColor: string
  plutoraReleaseType: string
  releaseProjectType: string
  releaseStatusType: string
  lastModifiedDate: Date
  parentReleaseID: null
  parentRelease: null
  releaseStatusTypeID: string
}

interface TypeAddInfo {
  id: string
  name: string
  type: string
  dataType: string
  text: string | null
  time: string | null
  date: string | null
  number: number | null
  decimal: number | null
  dateTime: string | null
  listItem: TypeListItem | null
  multiListItem: TypeListItem[] | null
}

interface TypeListItem {
  id: string
  value: string
  sortOrder: number
  type: string
}

interface TypeReleaseDetails {
  id: string | null
  identifier: string
  name: string
  summary: string
  releaseTypeId: string
  releaseType: string
  location: string
  releaseStatusTypeId: string
  releaseStatusType: string
  releaseRiskLevelId: string
  releaseRiskLevel: string
  implementationDate: string
  displayColor: string
  organizationId: string
  organization: string
  managerId: string
  manager: string
  parentReleaseId: string | null
  parentRelease: string | null
  plutoraReleaseType: string
  releaseProjectType: string
  additionalInformation: TypeAddInfo []
  lastModifiedDate: string
}

/**
 * @description get a release by its ID
 * @param {string} releaseID - The release identifier string
 * @returns {Promise} Returns the detailed release record for the supplied Identifier
 */
export const getReleaseByIdentifier = async function (
  releaseID: string
): Promise<TypeReleaseDetails> {
  let result: TypeRelease[] | null = null
  try {
    let temp: string = await plutoraWrapper.plutoraGetRequest(
      `releases?filter=(%20%60identifier%60%20equals%20%60${releaseID}%60%20)`
    )
    result = JSON.parse(temp)
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${releaseID}) with error ${err} >>>`
    )
  }
  let release: TypeRelease = result![0]
  let detailedResult: TypeReleaseDetails | null = null
  // get the detailed release info
  try {
    let temp = await plutoraWrapper.plutoraGetRequest(
      `releases/${release.id}`
    )
    detailedResult = JSON.parse(temp)
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${release.id}) with error ${err} >>>`
    )
  }
  // return the first entry in the array since there should only be one
  return detailedResult!
}

interface TypeTemplate {
  id: string
  identifier: string
  name: string
  summary: string
  releaseTypeId: string
  releaseType: string
  location: string
  releaseStatusTypeId: string
  releaseStatusType: string
  releaseRiskLevelId: string
  releaseRiskLevel: string
  implementationDate: string
  displayColor: string
  organizationId: string
  organization: string
  managerId: string
  manager: string
  parentReleaseId: string | null
  parentRelease: string
  plutoraReleaseType: string
  releaseProjectType: string
  stakeholders: [
    {
      id: "9279f511-7410-eb11-aa62-e6b22d29743d"
      userId: "2799ca7b-7301-eb11-aa62-e6b22d29743d"
      user: "Peter Gross"
      groupId: null
      group: ""
      stakeholderRoleIds: ["693d1db2-6c01-eb11-aa62-e6b22d29743d"]
      stakeholderRoles: "Release Manager"
      responsible: false
      accountable: true
      informed: false
      consulted: false
    }
  ]
  lastModifiedDate: string
}

interface TypeTemplateList {
  resultSet: TypeTemplate[]
  returnCount: number
  totalCount: number
  pageNum: number
  recordsPerPage: number
}

/**
 * @description create new Release in plutora
 * @param {string} templateName - The name of the template to retrieve
 * @returns {Promise} Returns the template release record from Plutora
 */
export const getReleaseTemplate = async (
  templateName: string
): Promise<TypeReleaseDetails> => {
  let result: TypeTemplateList | null = null
  try {
    let temp = await plutoraWrapper.plutoraGetRequest(
      `releasetemplates?filter=%60identifier%60%20equals%20%60${templateName}%60`
    )
    result = JSON.parse(temp)
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${templateName}) with error ${err} >>>`
    )
  }
  let template: TypeTemplate = result!.resultSet[0]
  let detailedResult: TypeReleaseDetails | null = null
  // get the detailed release info
  try {
    let temp = await plutoraWrapper.plutoraGetRequest(
      `releasetemplates/${template!.id}`
    )
    detailedResult = JSON.parse(temp)
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${
        template!.id
      }) with error ${err} >>>`
    )
  }
  // return the first entry in the array since there should only be one
  return detailedResult!
}

// get all changes
export const getAllReleases = async function () {
  return await plutoraWrapper.plutoraGetRequest( "/releases" )
}

// get all changes
export const getRelease = async function (id: string) {
  let release: any
  release = await plutoraWrapper.plutoraGetRequest( `/releases/${id}` )
  release.systems = await plutoraWrapper.plutoraGetRequest( `/releases/${id}/systems`)
  release.changes = await plutoraWrapper.plutoraGetRequest( `/releases/${id}/changes`)
  release.linked = await plutoraWrapper.plutoraGetRequest( `/releases/${id}/linkedItems`)
  release.criteria = await plutoraWrapper.plutoraGetRequest( `/releases/${id}/criterias`)
  
  // get the phases and return any booked environments and whether they are groups or environments
  let phases = await plutoraWrapper.plutoraGetRequest( `/releases/${id}/phases`)
  const result = await phases.map(async (item: any) => {
    item.name = plutoraWrapper.workItems[item.workItemNameID].name
    let environment = await plutoraWrapper.plutoraGetRequest( `/releases/${id}/phases/${item.id}/Environments`)
    item.environments = environment.map((env: any) => { 
      if (plutoraWrapper.envGroups[env.id]) {
        env.type = "Environment Group"
      } else {
        env.type = "Environment"
      }
      return env
    })
    return item
  })
  // have to wait for the promises in the map function to finish
  const addPhases = await Promise.all(result)
  release.phases = addPhases


  return(release)
}
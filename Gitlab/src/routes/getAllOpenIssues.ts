import express, { Request, Response } from "express"
import {gitlabWrapper} from "../gitlab-wrapper"
// import error handlers
import { BadRequestError } from "@relenv/common"
const router = express.Router()

router.get("/api/v1/gitlab/issues/:projectId", async (req: Request, res: Response) => {
  try {

    // query plutora for systems
    const issues = await gitlabWrapper.getOpenIssues(parseInt(req.params.groupId))

    res.send(issues)
  } catch (err) {
    throw new BadRequestError(`Failed to retrieve issues: ${err}`)
  }
})

export { router as indexGetOpenIssuesRouter }
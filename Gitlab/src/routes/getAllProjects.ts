import express, { Request, Response } from "express"
import {gitlabWrapper} from "../gitlab-wrapper"
// import error handlers
import { BadRequestError } from "@relenv/common"
const router = express.Router()

router.get("/api/v1/gitlab/projects/:groupId", async (req: Request, res: Response) => {
  try {

    // query plutora for systems
    const projects = await gitlabWrapper.getProjects(parseInt(req.params.groupId))

    res.send(projects)
  } catch (err) {
    throw new BadRequestError(`Failed to retrieve projects: ${err}`)
  }
})

export { router as indexGetProjectsRouter }
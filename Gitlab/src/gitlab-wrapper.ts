import axios from "axios"
import yaml from "js-yaml"

const keys = require('./config/keys')

//////////// Config ////////////
const config = {
  gitlab: {
    gitlabURI: keys.GITLAB_URI,
    gitlabToken: keys.GITLAB_TOKEN,
    gitlabGroup: 9524199,  // the petes_repo group
    gitlabUser: 5851736    // me
  },
}

// a wrapper class to connect to nats and be available to our whole application
class GitlabWrapper {
  // tell typescript that this may not exist
  public gitlabToken?: string = config.gitlab.gitlabToken
  public gitlabUser? : string
  public gitlabProjects? : any
  public currentProject? : number
  public currentGroup? : number = config.gitlab.gitlabGroup


  // connect to nats called once from index.ts
  async init() {

    try {
      this.gitlabProjects = await this.getProjects(config.gitlab.gitlabGroup)
      this.currentProject = 21432258 // set default project
      console.log(this)
      return "success"
    } catch (err) {
      return "error"
    }
  }

  // async setTimeout function to for API throttling
  // the sheer amount of systems they have, might need to extend the DELAY_RATE to avoid crapping out the API server

  async gitlabGetRequest(
    apipath: string
  ): Promise<any> {
    let data: any
    try {
      data = await axios.get(config.gitlab.gitlabURI + apipath, {
        headers: {
          'PRIVATE-TOKEN': this.gitlabToken,
          'Content-Type': 'application/json'
        }
      })
    } catch(err) {
      console.error(`<<< Error getting from ${config.gitlab.gitlabURI + apipath} >>>`)
      return(err)
    }
    return data.data
  };

  async gitlabPostRequest(
    apipath: string,
    data: any
  ): Promise<any> {
    let result: any
    try {
      result = await axios.post(config.gitlab.gitlabURI + apipath, data, {
        headers: {
          'PRIVATE-TOKEN': this.gitlabToken,
          'Accept': 'application/json',
        }
      })
    } catch(err) {
      console.error(`<<< Error getting from ${config.gitlab.gitlabURI + apipath} >>>`)
      return(err)
    }
    return data.data
  };

  async gitlabPutRequest(
    apipath: string,
    data: any
  ): Promise<any> {
    let result: any
    try {
      result = await axios.put(config.gitlab.gitlabURI + apipath, data, {
        headers: {
          'PRIVATE-TOKEN': this.gitlabToken,
          'Accept': 'application/json',
        }
      })
    } catch(err) {
      console.error(`<<< Error getting from ${config.gitlab.gitlabURI + apipath} >>>`)
      return(err)
    }
    return data.data
  };

  // return the projects for the group id passed in
  async getProjects(groupId: number) {
    try {
      const result: any = await this.gitlabGetRequest(
        `groups/${groupId}/projects`
      )
      return result.reduce((map: any, obj: any) => {
        map[obj.id] = obj
        return map
      }, {})
    } catch (err) {
      console.error(`<<< Error getting projects >>>`)
    }
  }

  // get a list of the files and directories in a path
  async getFilesList(projectId: number, path: string) {
    try {
      const result: any = await this.gitlabGetRequest(
        `projects/${projectId}/repository/tree?ref=master&path=${encodeURIComponent(path)}`
      )
      return result
    } catch (err) {
      console.error(`<<< Error getting filelist >>>`)
    }
  }

  // read a json file and return object from master branch
  async getFileContentJSON(projectId: number, path: string) {
    try {
      const query: any = await this.gitlabGetRequest(
        `projects/${projectId}/repository/files/${encodeURIComponent(path)}?ref=master`
      )
      const base64Content = Buffer.from(query.Content, 'base64')
      const content = base64Content.toString('utf8').replace(/\r?\n|\r/g, "")
      const result = JSON.parse(content) 
      return result
    } catch (err) {
      console.error(`<<< Error getting json file ${path} >>>`)
    }
  }

  // read a json file and return array of strings for each line from master branch
  async getFileContentDockerfile(projectId: number, path: string) {
    try {
      const query: any = await this.gitlabGetRequest(
        `projects/${projectId}/repository/files/${encodeURIComponent(path)}?ref=master`
      )
      const base64Content = Buffer.from(query.Content, 'base64')
      const content = base64Content.toString('utf8').replace(/\r?\n|\r/g, "")
      const result = content.split('\r\n')
      return result
    } catch (err) {
      console.error(`<<< Error getting dockerfile ${path} >>>`)
    }
  }

  // read a json file and return array of objects for each definition from master branch
  async getFileContentYAMLFile(projectId: number, path: string) {
    try {
      const query: any = await this.gitlabGetRequest(
        `projects/${projectId}/repository/files/${encodeURIComponent(path)}?ref=master`
      )
      // read in the base64 content
      const base64Content = Buffer.from(query.Content, 'base64')
      // convert to utf8
      const content = base64Content.toString('utf8')
      // convert yaml format to array of objects 
      const result = yaml.loadAll(content)
      return result
    } catch (err) {
      console.error(`<<< Error getting yaml file ${path} >>>`)
    }
  }

  async getOpenIssues(projectId: number) {
    try {
      const result: any = await this.gitlabGetRequest(
        `projects/${projectId}/issues?state=opened`
      )
      return result
    } catch (err) {
      console.error(`<<< Error getting the open issues >>>`)
    }
  }

  // get an issue with the iid i.e. 1,2,3 etc... not the issue id
  async getProjectIssue(projectId: number, issueIid: string) {
    try {
      const result: any = await this.gitlabGetRequest(
        `projects/${projectId}/issues/${issueIid}`
      )
      return result
    } catch (err) {
      console.error(`<<< Error getting the project issue >>>`)
    }
  }

  async getMilestones(projectId: number) {
    try {
      const result: any = await this.gitlabGetRequest(
        `projects/${projectId}/milestones`
      )
      return result
    } catch (err) {
      console.error(`<<< Error getting the open issues >>>`)
    }
  }

  async getLabels(projectId: number) {
    try {
      const result: any = await this.gitlabGetRequest(
        `projects/${projectId}/labels`
      )
      return result
    } catch (err) {
      console.error(`<<< Error getting the project labels >>>`)
    }
  }
}

// export an instance of the wrapper - which will be the same for the whole app
export const gitlabWrapper = new GitlabWrapper()

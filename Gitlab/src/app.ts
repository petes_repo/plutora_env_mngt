import express from "express"
// import a library which allows us to throw errors in async route handlers
import "express-async-errors"
import { json } from "body-parser"
import cookieSession from "cookie-session"
import swStats from "swagger-stats"
import mongoSanitize from "express-mongo-sanitize"
import helmet from "helmet"
import xss from "xss-clean"
import hpp from "hpp"
import cors from "cors"
// import local modules
import { errorHandler, NotFoundError, currentUser } from "@relenv/common"

// import routes
import { healthzRouter } from "./routes/healthz"

require("events").EventEmitter.defaultMaxListeners = 25

// setup express app
const app = express()
app.set("trust proxy", true) // ingress service acts as a proxy so add this so express works

// setup standard express middleware
app.use(json())
app.use(
  cookieSession({
    signed: false,
    secure: false, //process.env.NODE_ENV !== "test",
  })
)

// add the swagger and prometheus metrics
// /api/plutora/ui  for the swagger stats ui
// /api/plutora/metrics for the prometheus endpoint
app.use(
  swStats.getMiddleware({
    uriPath: "/api/v1/plutora",
  })
)
// Sanitize data
app.use(mongoSanitize());

// Set security headers
app.use(helmet());

// Prevent XSS attacks
app.use(xss());

// Prevent http param pollution
app.use(hpp());

// Enable CORS
app.use(cors());
// attach the current user to the request body on all calls
app.use(currentUser)
// routes for the service
app.use(healthzRouter)

// handle routes we were not expectinng
app.all("*", async () => {
  throw new NotFoundError()
})
// handle any thrown errors
app.use(errorHandler)

export { app }

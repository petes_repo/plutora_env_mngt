import React, { lazy, Suspense, useState, useEffect } from "react"
import { Router, Route, Switch, Redirect } from "react-router-dom"
import {
  StylesProvider,
  createGenerateClassName,
  ThemeProvider,
} from "@material-ui/core/styles"
import { createBrowserHistory } from "history"
import axios from "axios"

import Header from "./components/Header"
import Progress from "./components/Progress"
import Footer from "./components/Footer"
import LandingPage from "./components/LandingPage"

import theme from "./components/Theme"
import ErrorBoundary from "./components/ErrorBoundary"
// setup lazy loading for our remote components
//const PlutoraLazy = lazy(() => import('./components/PlutoraApp'))
const AuthLazy = lazy(() => import("./components/AuthApp"))
//const ReleasesLazy = lazy(() => import('./components/ReleasesApp'))
// make sure the generated class names are unique in each app by giving them a unique prefix
const generateClassName = createGenerateClassName({
  productionPrefix: "co",
  seed: "c",
})

const history = createBrowserHistory()

// use suspense to lazy load the apps for the routes
export default () => {
  const [currentUser, setCurrentUser] = useState(null)
  const [value, setValue] = useState(0)

  const fetchUser = async () => {
    try {
      const { data } = await axios.get("/api/v1/users/currentuser")
      console.log(`current user returned ${data}`)
      if (data) {
        setCurrentUser(data.currentUser)
      }
    } catch (err) {}
  }

  // when application loads check to see if there is a cookie
  useEffect(() => {
    fetchUser()
  }, [])
  // 
  return (
    <StylesProvider generateClassName={generateClassName}>
      <ThemeProvider theme={theme}>
        <ErrorBoundary>
          <Router history={history}>
            <Header
              currentUser={currentUser}
              setCurrentUser={setCurrentUser}
              value={value}
              setValue={setValue}
            />
            <Suspense fallback={<Progress />}>
              <Switch>
                <Route path="/users">
                  <AuthLazy
                    currentUser={currentUser}
                    setCurrentUser={setCurrentUser}
                    value={value}
                    setValue={setValue}
                    theme={theme}
                  />
                </Route>
                <Route exact path="/">
                  <LandingPage setValue={setValue} />
                </Route>
              </Switch>
            </Suspense>
            <Footer setValue={setValue} />
          </Router>
        </ErrorBoundary>
      </ThemeProvider>
    </StylesProvider>
  )
}

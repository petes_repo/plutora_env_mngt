import { mount } from 'auth/AuthApp'
import React, { useRef, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

// wrap the remote component so it can be used locally and handle navigation changes
export default ({ currentUser, setCurrentUser, value, setValue, theme }) => {
  // get reference to current element
  const ref = useRef(null)
  // get the history object for the container
  const history = useHistory()

  // call mount on component load only passing in current element
  useEffect(() => {
    const { onParentNavigate } = mount(ref.current, {
      initialPath: history.location.pathname,
      onNavigate: ({pathname: nextPathname}) => {
        const { pathname } = history.location
        // check the paths to see whether they need to change, if not we will get an infinite loop
        if (pathname !== nextPathname) {
          console.log(`onNavigate: ${pathname} -> ${nextPathname}`)
          history.push(nextPathname)
        }
      },
      currentUser,
      setCurrentUser,
      value,
      setValue, 
      theme
    })
    // when the parent navigates send the change to the child
    history.listen(onParentNavigate)
  }, [] )

  return <div ref={ref} />
}
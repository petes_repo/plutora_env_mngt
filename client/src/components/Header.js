import React, { useState }  from "react"
import AppBar from "@material-ui/core/AppBar"
import Button from "@material-ui/core/Button"
import Toolbar from "@material-ui/core/Toolbar"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import { makeStyles, useTheme } from "@material-ui/core/styles"
import { Link as RouterLink } from "react-router-dom"
import useMediaQuery from "@material-ui/core/useMediaQuery"
import logo from "../assets/logo.svg"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer"
import IconButton from "@material-ui/core/IconButton"
import MenuIcon from "@material-ui/icons/Menu"
// used to style the header and move the body down so the header does not cover it
// used to style html using classes
const useStyles = makeStyles((theme) => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
    marginBottom: "2.15em",
    [theme.breakpoints.down("md")]: {
      marginBottom: "1.5em"
    },
    [theme.breakpoints.down("xs")]: {
      marginBottom: "1.4em",
    },
  },
  logo: {
    height: "7em",
    [theme.breakpoints.down("md")]: {
      height: "6em",
    },
    [theme.breakpoints.down("xs")]: {
      height: "5.2em",
    },
  },
  tabContainer: {
    marginLeft: "auto", // pushes the tabs to the right auto uses as much space as it can
  },
  tab: {
    ...theme.typography.tab,
    minWidth: 10,
    marginRight: "25px",
  },
  button: {
    ...theme.typography.estimate,
    borderRadius: "50px",
    marginLeft: "50px",
    marginRight: "25px",
    height: "45px",
    "&:hover": {
      backgroundColor: theme.palette.secondary.light,
    },
  },
  logoContainer: {
    padding: 0,
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  menu: {
    backgroundColor: theme.palette.primary.main,
    color: "white",
    borderRadius: "0px",
  },
  menuItem: {
    ...theme.typography.tab,
    opacity: 0.7,
    "&:hover": {
      opacity: 1,
    },
  },
  drawIconContainer: {
    marginLeft: "auto",
    color: "white",
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  drawIcon: {
    height: "50px",
    width: "50px",
  },
  drawer: {
    backgroundColor: theme.palette.common.blue,
    zIndex: theme.zIndex.modal + 1,
  },
  drawerItem: {
    ...theme.typography.tab,
    color: "white",
    opacity: 0.6,
  },
  drawerItemEstimate: {
    backgroundColor: theme.palette.common.green,
  },
  drawerItemSelected: {
    "& .MuiListItemText-root": {
      opacity: 1,
    },
  },
  appbar: {
    zIndex: theme.zIndex.modal + 1,
  },
}))

export default function Header({
  currentUser,
  setCurrentUser,
  value,
  setValue,
}) {
  const classes = useStyles()
  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.down("md"))

  const [openDrawer, setOpenDrawer] = useState(false)

  const handleChange = (e, newValue) => {
    setValue(newValue)
  }
  
  console.log(currentUser)
  const links = [
    { label: "Home", to: "/", activeIndex: 0 },
    !currentUser && { label: "Sign Up", to: "/users/signup", activeIndex: 1 },
    !currentUser && { label: "Sign In", to: "/users/signin", activeIndex: 2 },
    currentUser && { label: "Users", to: "/users", activeIndex: 3 },
    currentUser && { label: "Sign Out", to: "/users/signout", activeIndex: 4 },
  ]
    .filter((linkConfig) => linkConfig)

  // we are displaying the menu as tabs in the header on medium size and up
  const tabs = (
    <React.Fragment>
      <Tabs
        value={value}
        onChange={handleChange}
        className={classes.tabContainer}
      >
        {links.map((route, index) => (
          <Tab
            key={`${index}`}
            className={classes.tab}
            component={RouterLink}
            to={route.to}
            label={route.label}
          />
        ))}
      </Tabs>
    </React.Fragment>
  )
  // below medium we have a dropdown menu drawer as the header menu
  const drawer = (
    <React.Fragment>
      <SwipeableDrawer
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
        onOpen={() => setOpenDrawer(true)}
        classes={{ paper: classes.drawer }}
      >
        <div className={classes.toolbarMargin} />
        <List disablePadding>
          {links.map((route) => (
            <ListItem
              onClick={() => {
                setOpenDrawer(false)
                setValue(route.activeIndex)
              }}
              key={`${route.activeIndex}`}
              divider
              button
              component={RouterLink}
              to={route.to}
              selected={value === route.activeIndex}
              classes={{ selected: classes.drawerItemSelected }}
            >
              <ListItemText className={classes.drawerItem} disableTypography>
                {route.label}
              </ListItemText>
            </ListItem>
          ))}
        </List>
      </SwipeableDrawer>
      <IconButton
        className={classes.drawIconContainer}
        onClick={() => setOpenDrawer(!openDrawer)}
        disableRipple
      >
        <MenuIcon className={classes.drawIcon} />
      </IconButton>
    </React.Fragment>
  )
  // show the header
  return (
    <React.Fragment>
      <AppBar position="fixed" color="primary" className={classes.appBar}>
        <Toolbar disableGutters>
          <Button
            component={RouterLink}
            to="/"
            disableRipple
            onClick={() => {
              setValue(0)
            }}
            className={classes.logoContainer}
          >
            <img src={logo} alt="company logo" className={classes.logo} />
          </Button>
          {matches ? drawer : tabs}
        </Toolbar>
      </AppBar>
      <div className={classes.toolbarMargin} />
    </React.Fragment>
  )
}

const { merge } = require("webpack-merge")
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin")

const packageJson = require("../package.json")
const commonConfig = require("./webpack.common")

const domain = process.env.SERVER_URL_BASE

const prodConfig = {
  mode: "production",
  output: {
    filename: "[name].[contenthash].js",
    publicPath: "/",
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "client",
      remotes: {
        // auth@ matches name: 'auth' in the auth webpack.config.js file
        auth: "auth@/gui/users/remoteEntry.js",
      },
      shared: packageJson.dependencies,
    }),
  ],
}

module.exports = merge(commonConfig, prodConfig)
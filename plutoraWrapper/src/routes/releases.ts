import express, { Request, Response } from "express"
import { plutoraWrapper } from "../plutora-wrapper"
// import error handlers
import { BadRequestError } from "@relenv/common"
import {
  getRelease,
  getAllReleases,
  createNewRelease,
  CreateEnterpriseRelease,
  CreateEnterpriseProjectRelease,
} from "../services/release"
import ICreateRelease from "../interfaces/IRelease"

const router = express.Router()

router.get(
  "/api/v1/plutora/releases/:id",
  async (req: Request, res: Response) => {
    try {
      // open a transaction with plutora
      await plutoraWrapper.token()

      // query plutora for systems
      const releases = await getRelease(req.params.id)

      res.send(releases)
    } catch (err) {
      throw new BadRequestError(`Failed to retrieve releases: ${err}`)
    }
  }
)

router.get("/api/v1/plutora/releases", async (req: Request, res: Response) => {
  try {
    // open a transaction with plutora
    await plutoraWrapper.token()

    // query plutora for systems
    const releases = await getAllReleases()

    res.send(releases)
  } catch (err) {
    throw new BadRequestError("Failed to retrieve releases")
  }
})

router.post("/api/v1/plutora/releases", async (req: Request, res: Response) => {
  try {
    // get the data from the body
    // example entry for the user fields
    //     "userFields": [ { "name": "Business Objective", "values": [ "Increase Sales" ]},
    //                     { "name": "Categories", "values": ["Microservice", "Javascript"] }]
    const { body } = req
    console.log(body)
    // open a transaction with plutora
    await plutoraWrapper.token()

    // query plutora for systems
    const releases = await createNewRelease(body)

    res.send(releases)
  } catch (err) {
    throw new BadRequestError("Failed to post releases")
  }
})

router.post(
  "/api/v1/plutora/releases/enttemplate",
  async (req: Request, res: Response) => {
    try {
      // get the data from the body
      // example entry for the user fields
      //     "userFields": [ { "name": "Business Objective", "values": [ "Increase Sales" ]},
      //                     { "name": "Categories", "values": ["Microservice", "Javascript"] }]
      const {
        template,
        data,
      }: { template: string; data: ICreateRelease } = req.body
      console.log(data)
      // open a transaction with plutora
      await plutoraWrapper.token()

      // query plutora for systems
      const releases = await CreateEnterpriseRelease(template, data)

      res.send(releases)
    } catch (err) {
      throw new BadRequestError(`Failed to post releases: ${err}`)
    }
  }
)

router.post(
  "/api/v1/plutora/releases/entprjtemplate",
  async (req: Request, res: Response) => {
    try {
      // get the data from the body
      // example entry for the user fields
      //     "userFields": [ { "name": "Business Objective", "values": [ "Increase Sales" ]},
      //                     { "name": "Categories", "values": ["Microservice", "Javascript"] }]
      const {
        template,
        data,
        parentRelease,
      }: {
        template: string
        data: ICreateRelease
        parentRelease: string
      } = req.body
      console.log(data)
      // open a transaction with plutora
      await plutoraWrapper.token()

      // query plutora for systems
      const releases = await CreateEnterpriseProjectRelease(template, data, parentRelease)

      res.send(releases)
    } catch (err) {
      throw new BadRequestError(`Failed to post releases: ${err}`)
    }
  }
)

export { router as indexReleasesRouter }

import express, { Request, Response } from "express"
import {plutoraWrapper} from "../plutora-wrapper"
// import error handlers
import { BadRequestError } from "@relenv/common"

const router = express.Router()

router.get("/api/v1/plutora/systems", async (req: Request, res: Response) => {
  try {
    // open a transaction with plutora
    await plutoraWrapper.token()

    // query plutora for systems
    const systems = await plutoraWrapper.systems

    res.send(systems)
  } catch (err) {
    throw new BadRequestError("Failed to retrieve systems")
  }
})

export { router as indexGetSystemRouter }

import express from "express"
// import a library which allows us to throw errors in async route handlers
import "express-async-errors"
import { json } from "body-parser"
import cookieSession from "cookie-session"
import swStats from "swagger-stats"

// import local modules
import { errorHandler, NotFoundError, currentUser } from "@relenv/common"

// import routes
import { healthzRouter } from "./routes/healthz"
import { indexReleasesRouter } from './routes/releases'

require("events").EventEmitter.defaultMaxListeners = 25

// setup express app
const app = express()
app.set("trust proxy", true) // ingress service acts as a proxy so add this so express works

// setup standard express middleware
app.use(json())
app.use(
  cookieSession({
    signed: false,
    secure: false, //process.env.NODE_ENV !== "test",
  })
)

// add the swagger and prometheus metrics
// /api/plutora/ui  for the swagger stats ui
// /api/plutora/metrics for the prometheus endpoint
app.use(
  swStats.getMiddleware({
    uriPath: "/api/v1/plutora",
  })
)

// attach the current user to the request body on all calls
app.use(currentUser)
// routes for the service
app.use(healthzRouter)
app.use(indexReleasesRouter)

// handle routes we were not expectinng
app.all("*", async () => {
  throw new NotFoundError()
})
// handle any thrown errors
app.use(errorHandler)

export { app }

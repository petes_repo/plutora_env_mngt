import mongoose from "mongoose"
// an interface that describes the properties that are required to create a new user
interface OrgAttrs {
  pl_id: string
  type: string
  name: string
}

// an interface that describes the build function for typescript
interface OrgModel extends mongoose.Model<OrgDoc> {
  build(attrs: OrgAttrs): OrgDoc
}

// an interface that describes the props that a user document has
export interface OrgDoc extends mongoose.Document {
  pl_id: string
  type: string
  name: string
}

// defines the mongoDB structure
// change the default json fields returned by redefining toJSON
const orgSchema = new mongoose.Schema(
  {
    pl_id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

// done this way to allow typescript to understand the attributes
// this is because you use User.build() instead of new User
orgSchema.statics.build = (attrs: OrgAttrs) => {
  return new Org(attrs)
}

// setup the model
const Org = mongoose.model<OrgDoc, OrgModel>("Org", orgSchema)

export { Org }

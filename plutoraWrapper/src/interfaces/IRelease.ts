export default interface ICreateRelease {
  identifier: string // short name of the release
  name: string // long name of the release
  summary: string // description of the release
  implementationDate: string // the implementation date in ISOString format
  plutoraReleaseType?: string // default Major
  releaseStatusType?: string // default Draft
  userFields?: Array<IUserField>
  assignTo?: string // defaults to pete.gross@btinternet.com to do
  systems?: Array<string>
  testPhases?: Array<ITestPhases>
}

// example entry for the user fields
//     "userFields": [ { "name": "Business Objective", "values": [ "Increase Sales" ]},
//                     { "name": "Categories", "values": ["Microservice", "Javascript"] }]
export interface IUserField {
  name: string
  values: string[]
}

export interface ITestPhases {
  phase: string
  testEnvironment: string
  startDate?: string
  endDate?: string
}

// used by the get releases interface
export interface IReleaseListItem {
  id: string
  identifier: string
  name: string
  implementationDate: string
  displayColor: string
  plutoraReleaseType: string
  releaseProjectType: string
  releaseStatusType: string
  lastModifiedDate: Date
  parentReleaseID: null
  parentRelease: null
  releaseStatusTypeID: string
}

export interface IAddInfo {
  id: string
  name: string
  type: string
  dataType: string
  text: string | null
  time: string | null
  date: string | null
  number: number | null
  decimal: number | null
  dateTime: string | null
  listItem: TypeListItem | null
  multiListItem: TypeListItem[] | null
}

export interface TypeListItem {
  id: string
  value: string
  sortOrder: number
  type: string
}

export interface IReleaseDetails {
  id: string | null
  identifier: string
  name: string
  summary: string
  releaseTypeId: string
  releaseType: string
  location: string
  releaseStatusTypeId: string
  releaseStatusType: string
  releaseRiskLevelId: string
  releaseRiskLevel: string
  implementationDate: string
  displayColor: string
  organizationId: string
  organization: string
  managerId: string
  manager: string
  parentReleaseId: string | null
  parentRelease: string | null
  plutoraReleaseType: string
  releaseProjectType: string
  additionalInformation: IAddInfo[]
  lastModifiedDate: string
}

export interface TypeTemplate {
  id: string
  identifier: string
  name: string
  summary: string
  releaseTypeId: string
  releaseType: string
  location: string
  releaseStatusTypeId: string
  releaseStatusType: string
  releaseRiskLevelId: string
  releaseRiskLevel: string
  implementationDate: string
  displayColor: string
  organizationId: string
  organization: string
  managerId: string
  manager: string
  parentReleaseId: string | null
  parentRelease: string
  plutoraReleaseType: string
  releaseProjectType: string
  stakeholders: [
    {
      id: "9279f511-7410-eb11-aa62-e6b22d29743d"
      userId: "2799ca7b-7301-eb11-aa62-e6b22d29743d"
      user: "Peter Gross"
      groupId: null
      group: ""
      stakeholderRoleIds: ["693d1db2-6c01-eb11-aa62-e6b22d29743d"]
      stakeholderRoles: "Release Manager"
      responsible: false
      accountable: true
      informed: false
      consulted: false
    }
  ]
  lastModifiedDate: string
}

export interface TypeTemplateList {
  resultSet: TypeTemplate[]
  returnCount: number
  totalCount: number
  pageNum: number
  recordsPerPage: number
}

export interface IPhaseListItem {
  id: string
  startDate: string
  endDate: string
  isIgnore: boolean
  isIgnoreChild: boolean
  workItemNameID: string
}

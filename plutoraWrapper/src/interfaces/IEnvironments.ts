export interface IListEnvGroups {
  resultSet: Array<IListEnvGroupItem>
  returnCount: number
  totalCount: number
  pageNum: number
  recordsPerPage: number
}

export interface IListEnvGroupItem {
  id: string
  name: string
  description: string
  color: string
  usageWorkItemId: string
  usageWorkItemName: string
  organizationId: string
  organizationName: string
  isAutoApproved: boolean
  displayBookingAlert: string
  bookingAlertMessage: string
  environmentIDs: Array<IFieldsListItem>
}

export interface IFieldsListItem {
  id: string
  name: string
}


export default interface ICreateSystem {
  name: string // long name of the release
  summary: string // description of the release
  implementationDate: string // the implementation date in ISOString format
  plutoraReleaseType?: string // default Major
  releaseStatusType?: string // default Draft
  userFields?: Array<IUserField>
  assignTo?: string // defaults to pete.gross@btinternet.com to do
  systems?: Array<string>
}
// example entry for the user fields
//     "userFields": [ { "name": "Business Objective", "values": [ "Increase Sales" ]},
//                     { "name": "Categories", "values": ["Microservice", "Javascript"] }]
export interface IUserField {
  name: string
  values: string[]
}

export interface ISysLinks {
  id: string
  name: string
}

export interface TypeListItem {
  id: string
  value: string
  sortOrder: number
  type: string
}

export interface IAddInfo {
  id: string
  name: string
  type: string
  dataType: string
  text: string | null
  time: string | null
  date: string | null
  number: number | null
  decimal: number | null
  dateTime: string | null
  listItem: TypeListItem | null
  multiListItem: TypeListItem[] | null
}

export interface ISystemFullDetails {
  id: string
  name: string
  systemAlias: ISysLinks[]
  vendor: string
  status: string
  organizationId: string
  organization: string
  description: string
  isAllowEdit: boolean
  inMyOrganization: boolean
  linkedEnvironments: ISysLinks[]
  additionalInformation: IAddInfo[]
  lastModifiedDate: string
  lastModifiedBy: string
}

export interface ISystemPostDetails {
  id: string
  name: string
  systemAlias: ISysLinks[]
  vendor: string
  status: string
  organizationId: string
  organization: string
  description: string
  isAllowEdit: boolean
  inMyOrganization: boolean
  linkedEnvironments: ISysLinks[]
  additionalInformation: IAddInfo[]
  lastModifiedDate: string
  lastModifiedBy: string
}

// example entry for the user fields
//     "userFields": [ { "name": "Dependencies", "values": [ "Depenedant upon the new AWS templates being defined" ]},
//                     { "name": "Gitlab_issue_id", "values": ["petes_repo/plutora_env_mngt#8"] }]
export interface ICreateChange {
  name: string
  description: string
  expectedDeliveryDate: string
  created_at: string
  status: string
  source?: string
  weight?: number
  systems?: string[]
  theme?: string
  type?: string
  release?: string
  userFields?: Array<IUserField>
}

export interface IChangeFullDetails {
  id: string
  name: string
  changeId: string
  changePriorityId: string
  changePriority: string
  changeStatusId: string
  changeStatus: string
  businessValueScore: number | null
  raisedById: string
  raisedBy: string
  assignedToId: string
  assignedTo: string
  raisedDate: string
  organizationId: string
  organization: string
  changeTypeId: string
  changeType: string
  changeDeliveryRiskId: string
  changeDeliveryRisk: string
  expectedDeliveryDate: string
  changeThemeId: string
  changeTheme: string
  description: string
  descriptionSimple: string
  lastModifiedDate: string
  lastModifiedBy: string
  additionalInformation: IAddInfo[]
  stakeholders: IStakeholders[]
  systems: IChgSystemList[]
  deliveryReleases: IChgReleaseList[]
  comments: IComment[]
}

export interface IComment {
  id: string
  userId: string
  user: string
  date: string
  text: string
  isDeleted: boolean
  childComments: IComment[]
}

export interface IChgReleaseList {
  releaseId: string
  releaseIdentifier: string
  releaseName: string
  enterpriseReleaseIdentifier: string
  implementationDate: string
  targetRelease: boolean
  actualDeliveryRelease: boolean
}

export interface IChangeFullPost {
  additionalInformation: IAddInfo[]
  stakeholders: IStakeholders[]
  systems: IChgSystemList[]
  comments: IChgCommentList[]
  name: string
  changePriorityId: string
  changeStatusId: string
  businessValueScore: number | null
  assignedToId: string
  raisedById: string
  organizationId: string
  changeTypeId: string
  changeDeliveryRiskId: string
  expectedDeliveryDate: string
  changeThemeId: string
  description: string
}

export interface IUserField {
  name: string
  values: string[]
}

export interface IChgCommentList {
  parentCommentId: string
  text: string
}

export interface IStakeholders {
  id: string
  userId: string
  groupId: string | null
  stakeholderRoleIds: string[]
  responsible: boolean
  accountable: boolean
  informed: boolean
  consulted: boolean
}

export interface IChgSystemList {
  systemId: string
  systemRoleType: string
}

export interface IAddInfo {
  id: string
  name: string
  type: string
  dataType: string
  text: string | null
  time: string | null
  date: string | null
  number: number | null
  decimal: number | null
  dateTime: string | null
  listItem: TypeListItem | null
  multiListItem: TypeListItem[] | null
}

export interface TypeListItem {
  id: string
  value: string
  sortOrder: number
  type: string
}

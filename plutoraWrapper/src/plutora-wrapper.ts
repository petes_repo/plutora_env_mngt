import querystring from "querystring"
import https from "https"
import axios from "axios"
import { FieldInstance } from "express-validator/src/base"
const keys = require("./config/keys")

//////////// Config ////////////
const config = {
  plutora: {
    oauthUrl: keys.PLUTORA_OAUTH_URL,
    clientId: keys.PLUTORA_CLIENT_ID,
    clientSecret: keys.PLUTORA_CLIENT_SECRET,
    username: keys.PLUTORA_USER_NAME,
    password: keys.PLUTORA_USER_PASSWORD,
    apiUrl: keys.PLUTORA_API_URL,
  },
}
//////////// Interfaces //////////
interface ILookup {
  id: string
  value: string
  sortOrder: number
  type: string
}
export interface ISystem {
  id: string
  name: string
  systemAlias: null
  vendor: string
  status: string
  organizationId: string
  organization: string
  description: string
  isAllowEdit: boolean
  inMyOrganization: boolean
  linkedEnvironments: Array<ILinkedEnv>
  additionalInformation: null
  lastModifiedDate: string
  lastModifiedBy: string
}

export interface ISystemItem {
  id: string
  status: string
  organizationId: string
  linkedEnvironments: Array<ILinkedEnv>
  additionalInformation: null
}

interface ILinkedEnv {
  id: string
  name: string
}
interface IRef {
  id: string
}
interface IOrg {
  id: string
  name: string
  directorId: null
  directorName: null
  type: string
}
interface IUser {
  id: string
  firstName: string
  lastName: string
  userName: string
  status: string
  roles: IRole[]
  phoneNumber: string
}
interface IRole {
  id: string
  name: string
  description: string
}
interface IWorkItem {
  id: string
  name: string
}
interface IEnvGroup {
  id: string
  name: string
  description: string
  color: string
  usageWorkItemId: string
  usageWorkItemName: string
  organizationId: string
  organizationName: string
  isAutoApproved: boolean
  displayBookingAlert: boolean
  bookingAlertMessage: string
  environmentIDs: ILinkedEnv[]
}
interface IEnvGroupItem {
  id: string
  name: string
  description: string
  usageWorkItemId: string
  environmentIDs: ILinkedEnv[]
}
interface IEnvGroupName {
  id: string
  environmentIDs: ILinkedEnv[]
}
interface ICustField {
  id: string
  value: string
  description: string
  descriptionDisplay: string
  dataType: string
  mandatory: boolean
  parentTabs: Array<string>
  customFieldGroupId: string
  customFieldGroupName: string
  customFieldGroupColor: string
}
export interface ICustomList {
  id: string
  value: string
  sortOrder: number
  type: string
}

// a wrapper class to connect to nats and be available to our whole application
class PlutoraWrapper {
  // tell typescript that this may not exist
  public plutoraAccessToken?: string
  public pullRequestChangeType?: { [name: string]: IRef } | undefined
  public deliveryRisks?: { [name: string]: IRef } | undefined
  public changeThemes?: { [name: string]: IRef } | undefined
  public changeType?: { [name: string]: IRef } | undefined
  public changePriorities?: { [name: string]: IRef } | undefined
  public changeStatuses?: { [name: string]: IRef } | undefined
  public releaseType?: { [name: string]: IRef } | undefined
  public releaseStatus?: { [name: string]: IRef } | undefined
  public releaseRiskLevel?: { [name: string]: IRef } | undefined
  public organisations?: { [name: string]: IOrg } | undefined
  public users?: { [name: string]: IUser } | undefined
  public systems?: { [name: string]: ISystemItem } | undefined
  public phases?: { [id: string]: { name: string} } | undefined
  public phaseToId?: { [name: string]: { id: string} } | undefined
  public envGroups?: { [name: string]: IEnvGroupItem } | undefined
  public envGroupName?: { [name: string]: IEnvGroupName } | undefined
  public releaseCategories?: { [name: string]: ICustomList } | undefined
  public envUsedFor?: { [name: string]: IRef } | undefined
  public stakeHolderRole?: { [name: string]: IRef } | undefined

  public releaseCustomFields?:
    | {
        [field: string]: {
          id: string
          type: string
          options?: { [name: string]: ICustomList }
        }
      }
    | undefined
  public changeCustomFields?:
    | {
        [field: string]: {
          id: string
          type: string
          options?: { [name: string]: ICustomList }
        }
      }
    | undefined
  private DELAY_RATE = 300
  // interfaces

  // return the token
  async token(): Promise<string> {
    return new Promise((resolve, reject) => {
      let body = ""
      let data = querystring.stringify({
        client_id: config.plutora.clientId,
        client_secret: config.plutora.clientSecret,
        grant_type: "password",
        username: config.plutora.username,
        password: config.plutora.password,
      })

      let options = {
        method: "POST",
        rejectUnauthorized: false,
        hostname: config.plutora.oauthUrl,
        path: "/oauth/token",
        headers: {
          "Content-Type": "x-www-form-urlencoded; charset=urf-8",
          Accept: "application/json",
        },
      }

      let req = https.request(options, (res) => {
        res.setEncoding("utf8")
        res.on("data", function (chunk) {
          body += chunk
        })
        res.on("end", () => {
          let result = JSON.parse(body)
          console.log("<<< Got Plutora Access Token >>>")
          this.plutoraAccessToken = result.access_token
          resolve(result.access_token)
        })
        res.on("error", (error) => {
          console.error(error)
          reject(error)
        })
      })
      req.write(data, "utf-8", (d) => {
        //console.log('Done flushing...', d);
      })
      req.end()
    })
  }

  // connect to nats called once from index.ts
  async init(): Promise<string> {
    this.plutoraAccessToken = await this.token()
    if (this.plutoraAccessToken !== "error") {
      this.pullRequestChangeType = await this.getLookupFields("ChangeType") // pullRequestChangeType["Pull Request"]
      this.deliveryRisks = await this.getLookupFields("ChangeDeliveryRisk") // deliveryRiskID['ChangeDeliveryRisk']
      this.changeThemes = await this.getLookupFields("ChangeTheme") // ChangeTheme
      this.changePriorities = await this.getLookupFields("ChangePriority") // ChangePriority
      this.changeStatuses = await this.getLookupFields("ChangeStatus") // ChangeStatus
      this.releaseType = await this.getLookupFields("ReleaseType")
      this.releaseStatus = await this.getLookupFields("ReleaseStatusType")
      this.releaseRiskLevel = await this.getLookupFields("ReleaseRiskLevel")
      this.organisations = await this.getOrganisations()
      this.users = await this.getUsers()
      this.envGroups = await this.getEnvGroups()
      this.envGroupName = await this.getEnvGroupsName()
      this.systems = await this.getSystems()
      this.phases = await this.getWorkitems()
      this.phaseToId = await this.getPhases()
      this.releaseCategories = await this.getReleaseCategories()
      this.envUsedFor = await this.getLookupFields("UsedForWorkItem")
      this.releaseCustomFields = await this.getReleaseCustomFields()
      this.changeCustomFields = await this.getChangeCustomFields()
      this.stakeHolderRole = await this.getLookupFields("StakeholderRole") // ChangePriority
      this.plutoraAccessToken = ""

      console.log(this)
      return "success"
    } else {
      return "error"
    }
  }

  // async setTimeout function to for API throttling
  // the sheer amount of systems they have, might need to extend the DELAY_RATE to avoid crapping out the API server

  async plutoraGetRequest(apipath: string): Promise<any> {
    let data: any
    try {
      data = await axios.get(config.plutora.apiUrl + apipath, {
        headers: {
          Authorization: `bearer ${this.plutoraAccessToken}`,
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
    } catch (err) {
      console.error(
        `<<< Error getting from ${config.plutora.apiUrl + apipath} >>>`
      )
      return err
    }
    return data.data
  }

  async plutoraPostRequest(apipath: string, data: any): Promise<any> {
    let result: any
    try {
      result = await axios.post(config.plutora.apiUrl + apipath, data, {
        headers: {
          Authorization: `bearer ${this.plutoraAccessToken}`,
          Accept: "application/json",
        },
      })
    } catch (err) {
      console.error(
        `<<< Error getting from ${config.plutora.apiUrl + apipath} ${err}>>>`
      )
      return err
    }
    return result.data
  }

  async plutoraPutRequest(apipath: string, data: any): Promise<any> {
    let result: any
    try {
      result = await axios.put(config.plutora.apiUrl + apipath, data, {
        headers: {
          Authorization: `bearer ${this.plutoraAccessToken}`,
          Accept: "application/json",
        },
      })
    } catch (err) {
      console.error(
        `<<< Error getting from ${config.plutora.apiUrl + apipath} >>>`
      )
      return err
    }
    return result.data
  }

  async getLookupFields(
    type: string
  ): Promise<{ [name: string]: IRef } | undefined> {
    try {
      const result: Array<ILookup> = await this.plutoraGetRequest(
        "lookupfields/" + type
      )
      return result.reduce((map: { [name: string]: IRef }, obj: ILookup) => {
        map[obj.value] = { id: obj.id }
        return map
      }, {})
    } catch (err) {
      console.error(`<<< Error getting lookupField for ${type} >>>`)
    }
  }

  async getOrganisations(): Promise<{ [name: string]: IOrg } | undefined> {
    try {
      const result: Array<IOrg> = await this.plutoraGetRequest("organizations")
      return result.reduce((map: { [name: string]: IOrg }, obj: IOrg) => {
        map[obj.name] = obj
        return map
      }, {})
    } catch (err) {
      console.error(`<<< Error getting Organisations >>>`)
    }
  }

  async getUsers(): Promise<{ [name: string]: IUser } | undefined> {
    try {
      const result: Array<IUser> = await this.plutoraGetRequest("users")
      return result.reduce((map: { [name: string]: IUser }, obj: IUser) => {
        map[obj.userName] = obj
        return map
      }, {})
    } catch (err) {
      console.error(`<<< Error getting users >>>`)
    }
  }

  async getSystems(): Promise<{ [name: string]: ISystemItem } | undefined> {
    try {
      const result: Array<ISystem> = await this.plutoraGetRequest(
        "systems/fulldetails"
      )
      return result.reduce((map: { [name: string]: ISystemItem }, obj: ISystem) => {
        map[obj.name] = {
          id: obj.id,
          status: obj.status,
          organizationId: obj.organizationId,
          linkedEnvironments: obj.linkedEnvironments,
          additionalInformation: null 
        }
        return map
      }, {})
    } catch (err) {
      console.error(`<<< Error getting systems ${err}>>>`)
    }
  }

  async getReleaseCategories(): Promise<
    { [name: string]: ICustomList } | undefined
  > {
    try {
      const result: Array<ICustomList> = await this.plutoraGetRequest(
        "customfields/f3989a5c-1076-eb11-aa67-89ba7747ce2e/customList"
      )
      return result.reduce(
        (map: { [name: string]: ICustomList }, obj: ICustomList) => {
          map[obj.value] = obj
          return map
        },
        {}
      )
    } catch (err) {
      console.error(`<<< Error getting Release Categories >>>`)
    }
  }

  async getWorkitems(): Promise<{ [id: string]: { name: string} } | undefined> {
    try {
      const result: Array<IWorkItem> = await this.plutoraGetRequest(
        "workitemnames/phases"
      )
      return result.reduce(
        (map: { [id: string]: { name: string} }, obj: IWorkItem) => {
          map[obj.id] = { name: obj.name }
          return map
        },
        {}
      )
    } catch (err) {
      console.error(`<<< Error getting workitem names >>>`)
    }
  }

  async getPhases(): Promise<{ [name: string]: { id: string} } | undefined> {
    try {
      const result: Array<IWorkItem> = await this.plutoraGetRequest(
        "workitemnames/phases"
      )
      return result.reduce(
        (map: { [name: string]: { id: string} }, obj: IWorkItem) => {
          map[obj.name] = { id: obj.id }
          return map
        },
        {}
      )
    } catch (err) {
      console.error(`<<< Error getting workitem names >>>`)
    }
  }

  async getEnvGroupsName(): Promise<{ [name: string]: IEnvGroupName } | undefined> {
    let envs: {[id: string]: {name: string}}
    try {
      const result: Array<IEnvGroup> = await this.plutoraGetRequest(
        "environmentgroups"
      )
      return result.reduce(
        (map: { [name: string]: IEnvGroupName }, obj: IEnvGroup) => {
          envs = {}
          for (let i = 0; i < obj.environmentIDs.length; i++) {
            envs[obj.environmentIDs[i].id] = { name: obj.environmentIDs[i].name }
          }

          map[obj.name] = {
            id: obj.id,
            environmentIDs: obj.environmentIDs
          }
          return map
        },
        {}
      )
    } catch (err) {
      console.error(`<<< Error getting workitem names >>>`)
    }
  }

  async getEnvGroups(): Promise<{ [name: string]: IEnvGroupItem } | undefined> {
    let envs: {[id: string]: {name: string}}
    try {
      const result: Array<IEnvGroup> = await this.plutoraGetRequest(
        "environmentgroups"
      )
      return result.reduce(
        (map: { [name: string]: IEnvGroupItem }, obj: IEnvGroup) => {
          envs = {}
          for (let i = 0; i < obj.environmentIDs.length; i++) {
            envs[obj.environmentIDs[i].id] = { name: obj.environmentIDs[i].name }
          }

          map[obj.id] = {
            id: obj.id,
            name: obj.name,
            description: obj.description,
            usageWorkItemId: obj.usageWorkItemId,
            environmentIDs: obj.environmentIDs
          }
          return map
        },
        {}
      )
    } catch (err) {
      console.error(`<<< Error getting workitem names >>>`)
    }
  }

  async getReleaseCustomFields(): Promise<{
    [field: string]: {
      id: string
      type: string
      options?: { [name: string]: ICustomList }
    }
  }> {
    let custFields: Array<ICustField> = []
    let i: number
    let answer: {
      [field: string]: {
        id: string
        type: string
        options?: { [name: string]: ICustomList }
      }
    } = {}
    try {
      custFields = await this.plutoraGetRequest(
        "customfields/ReleaseCustomField"
      )
    } catch (err) {
      console.error(`<<< Error getting custom fields >>>`)
    }
    let items: Array<ICustomList>
    let options: { [name: string]: ICustomList }
    for (i = 0; i < custFields.length; i++) {
      switch (custFields[i].dataType) {
        case "ListField":
          items = await this.plutoraGetRequest(
            `customfields/${custFields[i].id}/customList`
          )
          options = items.reduce(
            (map: { [name: string]: ICustomList }, obj: ICustomList) => {
              map[obj.value] = obj
              return map
            },
            {}
          )
          answer[custFields[i].value] = {
            id: custFields[i].id,
            type: "ListField",
            options,
          }
          break
        case "ListSelect":
          items = await this.plutoraGetRequest(
            `customfields/${custFields[i].id}/customList`
          )
          options = items.reduce(
            (map: { [name: string]: ICustomList }, obj: ICustomList) => {
              map[obj.value] = obj
              return map
            },
            {}
          )
          answer[custFields[i].value] = {
            id: custFields[i].id,
            type: "ListSelect",
            options,
          }
          break
        case "FreeText":
          answer[custFields[i].value] = {
            id: custFields[i].id,
            type: "FreeText",
          }
          break
        default:
          break
      }
    }
    return answer
  }

  async getChangeCustomFields(): Promise<{
    [field: string]: {
      id: string
      type: string
      options?: { [name: string]: ICustomList }
    }
  }> {
    let custFields: Array<ICustField> = []
    let i: number
    let answer: {
      [field: string]: {
        id: string
        type: string
        options?: { [name: string]: ICustomList }
      }
    } = {}
    try {
      custFields = await this.plutoraGetRequest(
        "customfields/ChangeCustomField"
      )
    } catch (err) {
      console.error(`<<< Error getting custom fields >>>`)
    }
    let items: Array<ICustomList>
    let options: { [name: string]: ICustomList }
    for (i = 0; i < custFields.length; i++) {
      switch (custFields[i].dataType) {
        case "ListField":
          items = await this.plutoraGetRequest(
            `customfields/${custFields[i].id}/customList`
          )
          options = items.reduce(
            (map: { [name: string]: ICustomList }, obj: ICustomList) => {
              map[obj.value] = obj
              return map
            },
            {}
          )
          answer[custFields[i].value] = {
            id: custFields[i].id,
            type: "ListField",
            options,
          }
          break
        case "ListSelect":
          items = await this.plutoraGetRequest(
            `customfields/${custFields[i].id}/customList`
          )
          options = items.reduce(
            (map: { [name: string]: ICustomList }, obj: ICustomList) => {
              map[obj.value] = obj
              return map
            },
            {}
          )
          answer[custFields[i].value] = {
            id: custFields[i].id,
            type: "ListSelect",
            options,
          }
          break
        case "FreeText":
          answer[custFields[i].value] = {
            id: custFields[i].id,
            type: "FreeText",
          }
          break
        case "DateTimePicker":
          answer[custFields[i].value] = {
            id: custFields[i].id,
            type: "DateTimePicker",
          }
          break
        default:
          break
      }
    }
    return answer
  }
}

// export an instance of the wrapper - which will be the same for the whole app
export const plutoraWrapper = new PlutoraWrapper()

import mongoose from "mongoose"
const keys = require('./config/keys');
import { app } from "./app"
//import { natsWrapper } from "./nats-wrapper"
import { plutoraWrapper } from "./plutora-wrapper"
// Define listeners
console.log("nodeenv")
console.log(keys)
const start = async () => {
  console.log("Starting.......")
  // first check that all required environment variables are defined
  if (!keys.JWT_KEY) {
    throw new Error("JWT_KEY must be defined")
  }
  if (!keys.MONGO_URI) {
    throw new Error("MONGO_URI must be defined")
  }
  if (!keys.NATS_CLIENT_ID) {
    throw new Error("NATS_CLIENT_ID must be defined")
  }
  if (!keys.NATS_URL) {
    throw new Error("NATS_URL must be defined")
  }
  if (!keys.NATS_CLUSTER_ID) {
    throw new Error("NATS_CLUSTER_ID must be defined")
  }
  if (!keys.PLUTORA_OAUTH_URL) {
    throw new Error("PLUTORA_OAUTH_URL must be defined")
  }
  if (!keys.PLUTORA_CLIENT_ID) {
    throw new Error("PLUTORA_CLIENT_ID must be defined")
  }
  if (!keys.PLUTORA_CLIENT_SECRET) {
    throw new Error("PLUTORA_CLIENT_SECRET must be defined")
  }
  if (!keys.PLUTORA_USER_NAME) {
    throw new Error("PLUTORA_USER_NAME must be defined")
  }
  if (!keys.PLUTORA_USER_PASSWORD) {
    throw new Error("PLUTORA_USER_PASSWORD must be defined")
  }
  if (!keys.PLUTORA_API_URL) {
    throw new Error("PLUTORA_API_URL must be defined")
  }
  // connect to mongodb
  try {
    // connect to plutora api and load initial lookups
    await plutoraWrapper.init()
    // connect to nats streaming server
    // await natsWrapper.connect(
    //   process.env.NATS_CLUSTER_ID,
    //   process.env.NATS_CLIENT_ID,
    //   process.env.NATS_URL
    // )
    // // if the nats wrapper receives a close event close the ticketing system
    // natsWrapper.client.on("close", () => {
    //   console.log("NATS connection closed")
    //   process.exit()
    // })
    // // if the ticketing service is being closed tell nats
    // process.on("SIGINT", () => natsWrapper.client.close())
    // process.on("SIGTERM", () => natsWrapper.client.close())
    // // now start the NATs listeners

    // connect to mongoose
    // await mongoose.connect(keys.MONGO_URI, {
    //   useNewUrlParser: true,
    //   useUnifiedTopology: true,
    //   useCreateIndex: true,
    // })
    // console.log("Connected to mongodb")
  } catch (err) {
    console.error(err)
  }
  //start the service
  app.listen(3000, () => {
    console.log("Listening on port 3000 :)")
  })
}
start()

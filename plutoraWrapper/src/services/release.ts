import { plutoraWrapper, ICustomList, ISystemItem } from "../plutora-wrapper"
// import error handlers
import { BadRequestError } from "@relenv/common"

import ICreateRelease, {
  IUserField,
  IReleaseListItem,
  IAddInfo,
  IReleaseDetails,
  TypeTemplate,
  IPhaseListItem
} from "../interfaces/IRelease"
////////////////////////////////////////////////////////
//////// Plutora API data loading functions ////////

/**
 * @description create new Release in plutora
 * @param {object} data - The data to create a new release from
 * @param {string} data.identifier - short name of the release
 * @param {string} data.name - long name of the release
 * @param {string} data.summary - description of the release
 * @param {string} data.implementationDate - the release implementation date
 * @param {string} data.plutoraReleaseType - release type default Major
 * @param {string} data.releaseStatusType - status default Draft
 * @param {IUserField[]} data.userFields - categories
 * @returns {Promise} Returns the release record just created in Plutora
 */
export const createNewRelease = async (data: ICreateRelease) => {
  // vet mandatory input data the rest will be defaulted if not present
  if (!data.identifier) {
    throw new BadRequestError("Release identifier must be provided")
  }
  if (!data.name) {
    throw new BadRequestError("Release name must be provided")
  }
  if (!data.summary) {
    throw new BadRequestError("Release summary must be provided")
  }
  if (!data.implementationDate) {
    throw new BadRequestError("Release implementationDate must be provided")
  }

  let newReleaseData: IReleaseDetails | null = null
  try {
    // Create the js object to create the new record
    newReleaseData = mapNewRelease(data)
    // call the plutora api to create the new release
    let newRelease = await plutoraWrapper.plutoraPostRequest(
      "releases",
      newReleaseData
    )
    try {
      // now we need to assign stakeholders
      let releaseManager = {
        userId: plutoraWrapper!.users!["pete.gross@btinternet.com"].id,
        groupId: null,
        stakeholderRoleIds: [
          plutoraWrapper!.stakeHolderRole!["Release Manager"].id,
        ],
        responsible: false,
        accountable: true,
        informed: false,
        consulted: false,
      }
      // call the plutora api to create the releasemanager
      await plutoraWrapper.plutoraPostRequest(
        `releases/${newRelease.id}/stakeholders`,
        releaseManager
      )
    } catch (err) {
      console.error(`<<< Issue posting stakeholder with error ${err} >>>`)
      throw new BadRequestError("Issue posting stakeholder with error")
    }

    // assign any passed in systems to the release
    await addRelSystems(data, newRelease.id!)

    return newReleaseData
  } catch (err) {
    console.error(
      `<<< Issue posting release with (${newReleaseData}) with error ${err} >>>`
    )
    throw new BadRequestError("Issue creating release")
  }
  // need to publish that a new release record has been created
}

/**
 * @description get a release by its ID
 * @param {string} releaseID - The release identifier string
 * @returns {Promise} Returns the detailed release record for the supplied Identifier
 */
export const getReleaseByIdentifier = async function (
  releaseID: string
): Promise<IReleaseDetails | null> {
  let result: IReleaseListItem[] | null = null
  // with the passed in short name for release, get the record
  try {
    result = await plutoraWrapper.plutoraGetRequest(
      `releases?filter=identifier%20%3D%20${releaseID}`
    )
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${releaseID}) with error ${err} >>>`
    )
  }
  // check that we have a result
  if (result && result.length === 0) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${releaseID}) >>>`
    )
  }
  // get the first result ### there may be more than one ###
  let release: IReleaseListItem = result![0]
  let detailedResult: IReleaseDetails | null = null
  // get the detailed release info
  try {
    detailedResult = await plutoraWrapper.plutoraGetRequest(
      `releases/${release.id}`
    )
    // return the first entry in the array since there should only be one
    return detailedResult
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${release.id}) with error ${err} >>>`
    )
  }
}

/**
 * @description create new Enterprise Release in plutora
 * @param {string} templateName - The name of the template to retrieve
 * @param
 * @returns {Promise} Returns the template release record from Plutora
 */
export const CreateEnterpriseRelease = async (
  templateName: string,
  data: ICreateRelease
): Promise<any> => {
  //): Promise<IReleaseDetails> => {
  let template: TypeTemplate | null = null
  try {
    let temp = await plutoraWrapper.plutoraGetRequest(
      `releasetemplates?filter=identifier%20%3D%20${templateName}`
    )
    template = temp.resultSet[0]
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${templateName}) with error ${err} >>>`
    )
  }
  let detailedResult: IReleaseDetails | null = null
  // setup string to create new release
  let newRelease = {
    templateId: template?.id,
    identifier: data.identifier,
    name: data.name,
    summary: data.summary,
    implementationDate: data.implementationDate,
    organizationId: template?.organizationId,
    organization: template?.organization,
  }
  try {
    // call the plutora api to create the release
    detailedResult = await plutoraWrapper.plutoraPostRequest(
      `releasetemplates/createrelease`,
      newRelease
    )
  } catch (err) {
    console.error(
      `<<< Issue creating enterprise release from template with error ${err} >>>`
    )
    throw new BadRequestError("Issue creating release from template")
  }
  //************************************************************************************ */
  // from this point we have created the release so just need to report any further errors
  //************************************************************************************ */
  // we have created the release we now need to add the user defoned fields if present
  // note the api does not do this
  let output: IAddInfo[] = []

  if (data.userFields) {
    output = mapUserFields(data.userFields)

    try {
      // call the plutora api to create the releasemanager
      await plutoraWrapper.plutoraPutRequest(
        `releases/${detailedResult!.id}/additionalInformation`,
        output
      )
    } catch (err) {
      console.error(
        `<<< Issue creating user defined fields with error ${err} >>>`
      )
    }
  }
  // now need to add any passed in systems
  // assign any passed in systems to the release
  await addRelSystems(data, detailedResult!.id!)

  // if test phases are passed in then change the dates from the template

  // At the moment we only handle SIT the PAT testing with the PAT environment being common
  // now we have the systems, we need to provisionally book the sit test environment
  if (data.testPhases){
    await addEnvBookings(data, detailedResult!.id!)
  }

  // return the first entry in the array since there should only be one
  // return detailedResult!
  return detailedResult
}

const addEnvBookings = async ( data: ICreateRelease, id: string): Promise<void> => {
  //   This reads the systems and testphases on the input and books environments
  //   and the environment groups for each of the phases listed
  //   example input: 
  //   "systems": ["Service:PlutoraWrapper","Service:Auth"],
  //   "testPhases": [
  //     {
  //         "phase": "SIT",
  //         "testEnvironment": "Relenv_staging"
  //      }
  // ]
  let phases: Array<{ phase: string, phaseId: string, environment: string }> = []

  // get the phaseIds for the release
  try {
    let environment
    let temp: Array<IPhaseListItem> = await plutoraWrapper.plutoraGetRequest(
      `releases/${id}/phases`
    )

    // iterate through the defined test phases
    for (let i = 0; i < temp.length; i++) {
      // iterate through the supplied test phases to book
      if (data && data.testPhases){ 
        
        let phaseName = plutoraWrapper!.phases![temp[i].workItemNameID].name
        
        for (let j = 0; j < data.testPhases.length; j++) {
          if (data.testPhases[j].phase === phaseName) {
            environment = data!.testPhases![j].testEnvironment
            phases.push({phase: phaseName, phaseId: temp[i].id, environment })
          }
        }
      }
    }
    
    for (let i = 0; i < phases.length; i++) {

      try {
        // create the booking for the test environment group for this test phase
        await plutoraWrapper.plutoraPostRequest(
          `releases/${id}/phases/${phases[i].phaseId}/Environments/${plutoraWrapper!.envGroupName![phases[i].environment].id}`,
          ""
        )
      } catch (err) {
        console.error(
          `<<< Issue creating environment group booking with error ${err} >>>`
        )
        throw new BadRequestError("Issue creating environment group booking")
      }

      let envSystems: Array<string> = []
      // build an array of the environments
      plutoraWrapper!.envGroupName![phases[i].environment].environmentIDs.map((env) => {
        envSystems.push(env.name)
      })
      // iterate over the passed in systems to check whether any are present
      for (let j = 0; j < data.systems!.length; j++) {
        // map over the systems environments
         for (let k = 0; k < plutoraWrapper!.systems![data.systems![j]].linkedEnvironments.length; k++) {
          let sysenv = plutoraWrapper!.systems![data.systems![j]].linkedEnvironments[k]
          // check whether they are part of the environment group
          if (envSystems.includes(sysenv.name)) {
            // add a booking for the system
            try {
              // call the plutora api to create the release
              await plutoraWrapper.plutoraPostRequest(
                `releases/${id}/phases/${phases[i].phaseId}/Environments/${sysenv.id}`,
                ""
              )
            } catch (err) {
              console.error(
                `<<< Issue creating enterprise release from template with error ${err} >>>`
              )
              throw new BadRequestError("Issue creating release from template")
            }
          }
        }
      }
    }
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${id}) with error ${err} >>>`
    )
  }
  
}

export const CreateEnterpriseProjectRelease = async (
  templateName: string,
  data: ICreateRelease,
  parentIdentifier: string
): Promise<IReleaseDetails> => {
  // get the parent release record
  let parent: IReleaseListItem | null = null
  try {
    let temp = await plutoraWrapper.plutoraGetRequest(
      `releases?filter=identifier%20%3D%20${parentIdentifier}`
    )
    parent = temp[0]
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${templateName}) with error ${err} >>>`
    )
  }

  let template: TypeTemplate | null = null
  try {
    let temp = await plutoraWrapper.plutoraGetRequest(
      `releasetemplates?filter=identifier%20%3D%20${templateName}`
    )
    template = temp.resultSet[0]
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${templateName}) with error ${err} >>>`
    )
  }
  console.log(template)
  let detailedResult: IReleaseDetails | null = null

  // setup string to create new release
  let newRelease = {
    templateId: template?.id,
    identifier: data.identifier,
    name: data.name,
    summary: data.summary,
    implementationDate: data.implementationDate,
    organizationId: template?.organizationId,
    organization: template?.organization,
    parentReleaseId: parent?.id,
    parentRelease: parentIdentifier,
  }
  try {
    // call the plutora api to create the releasemanager
    detailedResult = await plutoraWrapper.plutoraPostRequest(
      `releasetemplates/createrelease`,
      newRelease
    )
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue creating enterprise release from template with error ${err} >>>`
    )
  }
  //************************************************************************************ */
  // from this point we have created the release so just need to report any further errors
  //************************************************************************************ */
  // we have created the release we now need to add the user defoned fields if present
  // note the api does not do this
  let output: IAddInfo[] = []

  if (data.userFields) {
    output = mapUserFields(data.userFields)
    try {
      // call the plutora api to create the releasemanager
      await plutoraWrapper.plutoraPutRequest(
        `releases/${detailedResult!.id}/additionalInformation`,
        output
      )
    } catch (err) {
      console.error(
        `<<< Issue creating user defined fields with error ${err} >>>`
      )
    }
  }
  // now need to add any passed in systems
  // assign any passed in systems to the release
  await addRelSystems(data, detailedResult!.id!)
  // need to make sure the parent record has the system and environment booked for it
  ///////////////////////////////////////////////////////
  ////////////////////  TODO  ///////////////////////////
  ///////////////////////////////////////////////////////

  // The test phases can be different from the parent but within bounds
  // look at changing the phase dates
  
  // return the first entry in the array since there should only be one
  return detailedResult!
}

// get all Releases
export const getAllReleases = async function (): Promise<
  Array<IReleaseListItem>
> {
  return await plutoraWrapper.plutoraGetRequest("/releases")
}

// get release
export const getRelease = async function (id: string) {
  let release: any
  release = await plutoraWrapper.plutoraGetRequest(`/releases/${id}`)
  release.systems = await plutoraWrapper.plutoraGetRequest(
    `/releases/${id}/systems`
  )
  release.changes = await plutoraWrapper.plutoraGetRequest(
    `/releases/${id}/changes`
  )
  release.linked = await plutoraWrapper.plutoraGetRequest(
    `/releases/${id}/linkedItems`
  )
  release.criteria = await plutoraWrapper.plutoraGetRequest(
    `/releases/${id}/criterias`
  )

  // get the phases and return any booked environments and whether they are groups or environments
  let phases = await plutoraWrapper.plutoraGetRequest(`/releases/${id}/phases`)
  const result = await phases.map(async (item: any) => {
    item.name = plutoraWrapper.phases![item.workItemNameID].name
    let environment = await plutoraWrapper.plutoraGetRequest(
      `/releases/${id}/phases/${item.id}/Environments`
    )
    item.environments = environment.map((env: any) => {
      if (plutoraWrapper.envGroups![env.id]) {
        env.type = "Environment Group"
      } else {
        env.type = "Environment"
      }
      return env
    })
    return item
  })
  // have to wait for the promises in the map function to finish
  const addPhases = await Promise.all(result)
  release.phases = addPhases

  return release
}

/**
 * @description create new Enterprise Release in plutora
 * @param {string} templateName - The name of the template to retrieve
 * @param
 * @returns {Promise} Returns the template release record from Plutora
 */
export const UpdateRelease = async (
  releaseIdentifier: string,
  data: ICreateRelease
): Promise<IReleaseDetails> => {
  // get the release record
  let release: IReleaseDetails | null = null
  try {
    let temp = await plutoraWrapper.plutoraGetRequest(
      `releases/${releaseIdentifier}`
    )
    release = temp[0]
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${releaseIdentifier}) with error ${err} >>>`
    )
  }

  let updateData = {
    id: release?.id,
    identifier: data.identifier,
    name: data.name,
    summary: data.summary,
    releaseTypeId: release!.releaseTypeId,
    releaseStatusTypeId:
      plutoraWrapper.releaseStatus![data.releaseStatusType!] ||
      release?.releaseStatusTypeId,
    releaseRiskLevelId: release?.releaseRiskLevelId,
    implementationDate: data.implementationDate,
    displayColor: "#008000",
    organizationId: release?.organizationId,
    plutoraReleaseType: release?.plutoraReleaseType,
    releaseProjectType: release?.releaseProjectType,
  }

  try {
    // call the plutora api to create the releasemanager
    release = await plutoraWrapper.plutoraPutRequest(
      `releases/${release?.id}`,
      updateData
    )
  } catch (err) {
    console.error(
      `<<< Issue creating enterprise release from template with error ${err} >>>`
    )
  }
  //************************************************************************************ */
  // from this point we have created the release so just need to report any further errors
  //************************************************************************************ */
  // we have created the release we now need to add the user defoned fields if present
  // note the api does not do this
  let output: IAddInfo[] = []

  if (data.userFields) {
    output = mapUserFields(data.userFields)
    try {
      // call the plutora api to create the releasemanager
      await plutoraWrapper.plutoraPostRequest(
        `releases/${release!.id}/additionalInformation`,
        output
      )
    } catch (err) {
      console.error(
        `<<< Issue creating user defined fields with error ${err} >>>`
      )
    }
  }
  // now need to add any passed in systems
  // assign any passed in systems to the release
  await addRelSystems(data, release!.id!)

  // return the first entry in the array since there should only be one
  return release!
}

// add passed in systems to the release
const addRelSystems = async function (
  data: ICreateRelease,
  id: string
): Promise<void> {
  let { systems } = data
  let systemDetails: ISystemItem

  if (systems) {
    for (let i = 0; i < systems.length; i++) {
      systemDetails = plutoraWrapper!.systems![systems[i]]
      // if the system was found then associate it with the release
      if (systemDetails) {
        try {
          // first associate the system with the release
          await plutoraWrapper.plutoraPostRequest(`releases/${id}/systems`, {
            "systemId": systemDetails.id,
            "systemRoleDependencyTypeId": "463d1db2-6c01-eb11-aa62-e6b22d29743d"          
          })

          // now default a production live deployment for the implementation date
          await plutoraWrapper.plutoraPostRequest(`releases/${id}/systems/${systemDetails.id}/deploymentDates`, {
            deploymentType: "Deployment",
            deploymentTypeID: "823d1db2-6c01-eb11-aa62-e6b22d29743d",
            deploymentTitle: "Production Deployment",
            deployingTo: "Production",
            deployingToID: "833d1db2-6c01-eb11-aa62-e6b22d29743d",
            enterpriseType: "Live",
            enterpriseTypeID: "67640119-4b83-eb11-aa6e-c20c63ea6612",
            startDate: data.implementationDate,
            endDate: data.implementationDate,
            changeRecord: null,
          })
        } catch (err) {
          console.log(`<< error saving system to release: ${err} >>>`)
        }
      } else {
        // otherwise we need to report it in case we are missing a system
        console.log(`<< error saving system: ${systems[i]} >>>`)
      }
    }
  }
}

// example entry for the user fields
//     "userFields": [ { "name": "Business Objective", "values": [ "Increase Sales" ]},
//                     { "name": "Categories", "values": ["Microservice", "Javascript"] }]
const mapUserFields = (fields: IUserField[]) => {
  let isValid = true
  let field:
    | {
        id: string
        type: string
        options?: { [name: string]: ICustomList }
      }
    | undefined

  // iterate over the array of user fields passed into the route
  return fields.map((option) => {
    // validate the userfield entry
    // find field in definition
    isValid = true
    field = plutoraWrapper.releaseCustomFields![option.name]
    // if the field was found
    if (field) {
      if (field.type === "ListField" || field.type === "ListSelect") {
        let invalidEntries = option.values.filter((item) => {
          field!.options![item] === undefined
        })
        if (invalidEntries.length > 0) {
          console.log(
            `Invalid option ${option.values.toString()} for ${option.name}`
          )
          isValid = false
        }
      }
    } else {
      isValid = false
      console.log(`Invalid custom field ${option.name}`)
    }
    // now we know it is valid to produce the additionalInformation section
    if (isValid) {
      let result: any
      switch (field.type) {
        case "ListField":
          result = {
            id: field.id || "bollocks",
            name: option.name,
            type: "Release",
            dataType: "ListField",
            text: null,
            time: null,
            date: null,
            number: null,
            decimal: null,
            dateTime: null,
            listItem: {
              id: field.options![option.values[0]].id || "bollocks",
              value: option.values[0],
              sortOrder: 0,
              type: "Release",
            },
            multiListItem: null,
          }
          break
        case "ListSelect":
          let items = []
          items = option.values.map((item) => {
            return {
              id: field!.options![item].id || "bollocks",
              value: field!.options![item].value,
              sortOrder: 0,
              type: "Release",
            }
          })
          result = {
            id: field.id || "bollocks",
            name: option.name,
            type: "Release",
            dataType: "ListSelect",
            text: null,
            time: null,
            date: null,
            number: null,
            decimal: null,
            dateTime: null,
            listItem: null,
            multiListItem: items,
          }
          break
        default:
          result = {
            id: field.id,
            name: option.name,
            type: "Release",
            dataType: "FreeText",
            text: option.values[0],
            time: null,
            date: null,
            number: null,
            decimal: null,
            dateTime: null,
            listItem: null,
            multiListItem: null,
          }
      }
      return result
    }
  })
}

// function to map pull requests to changes
const mapNewRelease = function (data: ICreateRelease): IReleaseDetails {
  // might want to retrieve a template and then alter it
  // build the user defined fields
  let output: IAddInfo[] = []

  if (data.userFields) {
    output = mapUserFields(data.userFields)
  }

  return {
    id: null,
    identifier: data.identifier,
    name: data.name,
    summary: data.summary,
    releaseTypeId: plutoraWrapper.releaseType![
      data.plutoraReleaseType || "Major"
    ].id,
    releaseType: data.plutoraReleaseType || "Major",
    location: "",
    releaseStatusTypeId: plutoraWrapper.releaseStatus![
      data.releaseStatusType || "Draft"
    ].id,
    releaseStatusType: data.releaseStatusType || "Draft",
    releaseRiskLevelId: plutoraWrapper.releaseRiskLevel!["Medium"].id,
    releaseRiskLevel: "Medium",
    implementationDate: data.implementationDate,
    displayColor: "#0000ff",
    organizationId: plutoraWrapper.organisations!["Plutora Integrations"].id,
    organization: "Plutora Integrations",
    managerId: plutoraWrapper.users!["pete.gross@btinternet.com"].id,
    manager: "Peter Gross",
    parentReleaseId: null,
    parentRelease: null,
    plutoraReleaseType: "Independent",
    releaseProjectType: "IsProject",
    additionalInformation: output,
    lastModifiedDate: new Date().toISOString(),
  }
}

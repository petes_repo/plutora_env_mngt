import { plutoraWrapper, ICustomList } from "../plutora-wrapper"
import { BadRequestError } from "@relenv/common"
import {
  IChangeFullPost,
  ICreateChange,
  IAddInfo,
  IChgSystemList,
  IChangeFullDetails,
} from "../interfaces/IChange"
import { IUserField } from "../interfaces/IRelease"
////////////////////////////////////////////////////////
//////// Plutora API data loading functions ////////

// @desc      get create new change in plutora
// @param     { }
// @access    Public
export const createNewChange = async (data: any) => {
  let newChangeData = {}
  try {
    newChangeData = mapNewChange(data)
    let newChange = await plutoraWrapper.plutoraPostRequest(
      "changes",
      newChangeData
    )
    return newChange
  } catch (err) {
    console.error(
      `<<< Issue posting change with (${newChangeData})  with error ${err} >>>`
    )
  }
}

// function to map pull requests to changes
const mapNewChange = async function (
  data: ICreateChange
): Promise<IChangeFullPost> {
  let changeStatusId: string
  let priority: string
  let addInfo: IAddInfo[] = []
  let chgSystems: IChgSystemList[] = []
  // vet the passed in data

  if (!data.release) {
    throw new BadRequestError("Release identifier must be provided")
  }
  if (!data.name) {
    throw new BadRequestError("change title must be provided")
  }
  if (!data.expectedDeliveryDate) {
    throw new BadRequestError("change due date must be provided")
  }
  if (!data.status) {
    throw new BadRequestError("change status must be provided")
  }
  if (!data.description) {
    throw new BadRequestError("change description must be provided")
  }
  // map the status from the source system
  if (data.source) {
    if (data.source === "GitLab") {
      changeStatusId =
        plutoraWrapper!.changeStatuses![data.status].id ||
        plutoraWrapper!.changeStatuses!["Opened"].id
    } else {
      changeStatusId = plutoraWrapper!.changeStatuses!["Draft"].id
    }
  } else {
    changeStatusId =
      plutoraWrapper!.changeStatuses![data.status].id ||
      plutoraWrapper!.changeStatuses!["Draft"].id
  }
  
  // map the weight to business priority
  if (data.weight) {
    if (data.weight === 0) {
      priority = plutoraWrapper!.changePriorities!["N/A"].id
    } else if (data.weight <= 3) {
      priority = plutoraWrapper!.changePriorities!["Low"].id
    } else if (data.weight <= 3) {
      priority = plutoraWrapper!.changePriorities!["Medium"].id
    } else {
      priority = plutoraWrapper!.changePriorities!["High"].id
    }
  } else {
    priority = plutoraWrapper!.changePriorities!["N/A"].id
  }

  if (data.userFields) {
    addInfo = mapUserFields(data.userFields)
  }

  if (data.systems) {
    chgSystems = mapChgSystems(data.systems)
  }

  return {
    name: data.name,
    changePriorityId: priority,
    changeStatusId: changeStatusId,
    businessValueScore: null,
    raisedById: "2799ca7b-7301-eb11-aa62-e6b22d29743d",
    organizationId: plutoraWrapper.organisations!["Plutora Integrations"].id,
    changeTypeId:
      plutoraWrapper.changeType![data!.type!].id ||
      plutoraWrapper.changeType!["Feature"].id,
    assignedToId: "6a55cf86-7782-eb11-aa6e-c20c63ea6612",
    changeDeliveryRiskId: plutoraWrapper.deliveryRisks!["Low"].id,
    expectedDeliveryDate: data!.expectedDeliveryDate,
    changeThemeId:
      plutoraWrapper.changeThemes![data!.theme!].id ||
      plutoraWrapper.changeThemes!["Strategic"].id,
    description: data.description,
    additionalInformation: addInfo,
    stakeholders: [
      {
        id: "c6d730a1-5809-eb11-aa62-e6b22d29743d",
        userId: "2799ca7b-7301-eb11-aa62-e6b22d29743d",
        groupId: null,
        stakeholderRoleIds: ["693d1db2-6c01-eb11-aa62-e6b22d29743d"],
        responsible: false,
        accountable: true,
        informed: false,
        consulted: false,
      },
    ],
    systems: chgSystems,
    comments: [],
  }
}

// add any passed in systems as impacted systems on the change
const mapChgSystems = (systems: Array<string>) => {
  let result: IChgSystemList
  return systems.map((system: string) => {
    result = {
      systemId: plutoraWrapper!.systems![system].id,
      systemRoleType: "Impact",
    }
    return result
  })
}

// example entry for the user fields
//     "userFields": [ { "name": "Author", "values": [ "Peter Gross" ]},
//                     { "name": "Gitlab_project_id", "values": ["21432258"] }
//                     { "name": "Raised", "values": ["2021-03-12T11:52:00"] }]
const mapUserFields = (fields: IUserField[]) => {
  let isValid = true
  let field:
    | {
        id: string
        type: string
        options?: { [name: string]: ICustomList }
      }
    | undefined

  // iterate over the array of user fields passed into the route
  return fields.map((option) => {
    // validate the userfield entry
    // find field in definition
    isValid = true
    field = plutoraWrapper.changeCustomFields![option.name]
    // if the field was found
    if (field) {
      if (field.type === "ListField" || field.type === "ListSelect") {
        let invalidEntries = option.values.filter((item) => {
          field!.options![item] === undefined
        })
        if (invalidEntries.length > 0) {
          console.log(
            `Invalid option ${option.values.toString()} for ${option.name}`
          )
          isValid = false
        }
      }
    } else {
      isValid = false
      console.log(`Invalid custom field ${option.name}`)
    }
    // now we know it is valid to produce the additionalInformation section
    if (isValid) {
      let result: any
      switch (field.type) {
        case "ListField":
          result = {
            id: field.id,
            name: option.name,
            type: "Change",
            dataType: "ListField",
            text: null,
            time: null,
            date: null,
            number: null,
            decimal: null,
            dateTime: null,
            listItem: {
              id: field.options![option.values[0]].id,
              value: option.values[0],
              sortOrder: 0,
              type: "Change",
            },
            multiListItem: null,
          }
          break
        case "ListSelect":
          let items = []
          items = option.values.map((item) => {
            return {
              id: field!.options![item].id,
              value: field!.options![item].value,
              sortOrder: 0,
              type: "Change",
            }
          })
          result = {
            id: field.id,
            name: option.name,
            type: "Change",
            dataType: "ListSelect",
            text: null,
            time: null,
            date: null,
            number: null,
            decimal: null,
            dateTime: null,
            listItem: null,
            multiListItem: items,
          }
          break
        case "DateTimePicker":
          result = {
            id: field.id,
            name: option.name,
            type: "Change",
            dataType: "DateTimePicker",
            text: null,
            time: null,
            date: null,
            number: null,
            decimal: null,
            dateTime: option.values[0],
            listItem: null,
            multiListItem: null,
          }
        default:
          result = {
            id: field.id,
            name: option.name,
            type: "Change",
            dataType: "FreeText",
            text: option.values[0],
            time: null,
            date: null,
            number: null,
            decimal: null,
            dateTime: null,
            listItem: null,
            multiListItem: null,
          }
      }
      return result
    }
  })
}

// get a change by its ID
export const getChangeByID = async function (
  changeID: string
): Promise<IChangeFullDetails | null> {
  let result: IChangeFullDetails

  try {
    result = await plutoraWrapper.plutoraGetRequest(`changes/${changeID}`)
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting change with id (${changeID}) with error ${err} >>>`
    )
  }

  return result
}

// get a change by its ID
export const getChangeByName = async function (changeName: string) {
  let result = ""

  try {
    result = await plutoraWrapper.plutoraGetRequest(
      `changes?filter=name%20%3D%20${changeName}`
    )
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting change with name (${changeName}) with error ${err} >>>`
    )
  }

  return result
}

// get linked changes
export const getChangesLinkedChanges = async function (changeID: string) {
  try {
    return await plutoraWrapper.plutoraGetRequest(
      `changes/${changeID}/linkedChanges`
    )
  } catch (err) {
    console.error(
      `<<< Issue getting linked changes (${changeID})  with error ${err} >>>`
    )
  }
}

// update linked changes
export const updateChangesLinkedChanges = async function (
  changeID: string,
  linkedChangesInfo: any
) {
  console.log(`<<< Updating linked changes of Change: (${changeID}) >>>`)
  try {
    return await plutoraWrapper.plutoraPutRequest(
      `changes/${changeID}/linkedChanges`,
      linkedChangesInfo
    )
  } catch (err) {
    console.error(
      `<<< Issue updating linked changes (${changeID})  with error ${err} >>>`
    )
  }
}

// update change by its ID
export const updateChange = async function (
  changeID: string,
  data: ICreateChange
) {
  let change: IChangeFullDetails
  let changeStatusId: string
  let priority: string
  let addInfo: IAddInfo[] = []
  let chgSystems: IChgSystemList[] = []
  try {
    let temp = await plutoraWrapper.plutoraGetRequest(`changes/${changeID}`)
    change = temp[0]
  } catch (err) {
    throw new BadRequestError(
      `<<< Issue getting release with identifier (${changeID}) with error ${err} >>>`
    )
  }

  // map the status from the source system
  if (data.source) {
    if (data.source === "GitLab") {
      changeStatusId =
        plutoraWrapper!.changeStatuses![data.status].id ||
        plutoraWrapper!.changeStatuses!["Opened"].id
    } else {
      changeStatusId = plutoraWrapper!.changeStatuses!["Draft"].id
    }
  } else {
    changeStatusId =
      plutoraWrapper!.changeStatuses![data.status].id ||
      plutoraWrapper!.changeStatuses!["Draft"].id
  }
  // map the weight to business priority
  if (data.weight) {
    if (data.weight === 0) {
      priority = plutoraWrapper!.changePriorities!["N/A"].id
    } else if (data.weight <= 3) {
      priority = plutoraWrapper!.changePriorities!["Low"].id
    } else if (data.weight <= 3) {
      priority = plutoraWrapper!.changePriorities!["Medium"].id
    } else {
      priority = plutoraWrapper!.changePriorities!["High"].id
    }
  } else {
    priority = plutoraWrapper!.changePriorities!["N/A"].id
  }

  if (data.userFields) {
    addInfo = mapUserFields(data.userFields)
  }

  if (data.systems) {
    chgSystems = mapChgSystems(data.systems)
  }

  let updateData = {
    id: changeID,
    name: data.name || change.name,
    changePriorityId: priority,
    changeStatusId: changeStatusId,
    businessValueScore: 100,
    raisedById: "2799ca7b-7301-eb11-aa62-e6b22d29743d",
    assignedToId: "6a55cf86-7782-eb11-aa6e-c20c63ea6612",
    organizationId: plutoraWrapper.organisations!["Plutora Integrations"].id,
    changeTypeId:
      plutoraWrapper.changeType![data!.type!].id ||
      plutoraWrapper.changeType!["Feature"].id,
    changeDeliveryRiskId: plutoraWrapper.deliveryRisks!["Low"].id,
    changeThemeId:
      plutoraWrapper.changeThemes![data!.theme!].id ||
      plutoraWrapper.changeThemes!["Strategic"].id,
    additionalInformation: addInfo,
    systems: chgSystems,
    expectedDeliveryDate:
      data!.expectedDeliveryDate || change.expectedDeliveryDate,
  }

  console.log(`<<< Updating change (${changeID}) >>>`)
  let Result
  try {
    Result = await plutoraWrapper.plutoraPutRequest(
      `changes/${changeID}`,
      updateData
    )
    return Result
  } catch (err) {
    console.error(
      `<<< Issue updating change (${changeID})  with error ${err} >>>`
    )
    return "Error"
  }
}

// get all changes
export const getAllChanges = async function () {
  return await plutoraWrapper.plutoraGetRequest("/changes")
}

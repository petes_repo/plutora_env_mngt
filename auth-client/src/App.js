import React from "react"
import { Switch, Route, Router, Redirect } from "react-router-dom"
import {
  StylesProvider,
  createGenerateClassName,
  ThemeProvider,
} from "@material-ui/core/styles"

import Signin from "./components/Signin"
import Signup from "./components/Signup"
import Signout from "./components/Signout"
import Users from "./components/Users"
import UpdateUser from "./components/Update.js"

export default ({
  currentUser,
  setCurrentUser,
  value,
  setValue,
  history,
  theme,
}) => {
  let isAdmin = false
  if (currentUser) {
    if (currentUser.role === "admin") {
      isAdmin = true
    }
  }
  console.log(currentUser)
  console.log(isAdmin)
  return (
    <div>
      <ThemeProvider theme={theme}>
        <Router history={history}>
          <Switch>
            <Route path="/users/signin">
              <Signin setCurrentUser={setCurrentUser} />
            </Route>
            <Route path="/users/signup">
              <Signup setCurrentUser={setCurrentUser} />
            </Route>
            <Route path="/users/signout">
              <Signout setCurrentUser={setCurrentUser} />
            </Route>
            <Route
              path="/users/:userId"
              render={(props) => (
                <UpdateUser {...props} currentUser={currentUser} />
              )}
            />
            <Route path="/users">
              <Users setValue={setValue} />
            </Route>
          </Switch>
        </Router>
      </ThemeProvider>
    </div>
  )
}

import { useCallback, useReducer } from "react"

// reducer function to update the state
const formReducer = (state, action) => {
  switch (action.type) {
    case "INPUT_CHANGE":
      // set form field state to true
      let formIsValid = true
      // iterate through the define inputs in the state
      for (const inputId in state.inputs) {
        // if the fields id does not exist in state exit
        if (!state.inputs[inputId]) {
          continue
        }
        // if the field that changed is the current input field
        if (inputId === action.inputId) {
          // update the form valid with new isValid value
          formIsValid = formIsValid && action.isValid
        } else {
          // update the form valid with existing isValid value
          formIsValid = formIsValid && state.inputs[inputId].isValid
        }
      }
      // now update the state of the form
      return {
        ...state,
        inputs: {
          ...state.inputs,
          [action.inputId]: { value: action.value, isValid: action.isValid, errors: action.errors },
        },
        isValid: formIsValid,
      }
    case "SET_DATA":
      return {
        inputs: action.inputs,
        isValid: action.formIsValid,
        errors: action.formErrors
      }
    case "FORM_CHANGE":
      return {
        ...state,
        errors: action.errors
      }
    default:
      return state
  }
}

// a form state hook
export const useForm = (initialInputs, initialFormValidity) => {
  const [formState, dispatch] = useReducer(formReducer, {
    inputs: initialInputs,
    isValid: initialFormValidity,
    errors: []
  })
  
  // useCallback stops the function being redelared on each render
  const formHandler = useCallback((errors) => {
    dispatch({
      type: "FORM_CHANGE",
      errors: errors
    })
  }, [])

  // useCallback stops the function being redelared on each render
  const inputHandler = useCallback((id, value, isValid, fieldErrors) => {
    dispatch({
      type: "INPUT_CHANGE",
      value: value,
      isValid: isValid,
      inputId: id,
      errors: fieldErrors
    })
  }, [])
  // useCallback stops the function being redelared on each render
  const setFormData = useCallback((inputData, formValidity, errors) => {
    dispatch({
      type: "SET_DATA",
      inputs: inputData,
      formIsValid: formValidity,
      formErrors: errors
    })
  }, [])

  return [formState, inputHandler, setFormData, formHandler]
}

import React, { useReducer, useEffect } from "react"
import TextField from '@material-ui/core/TextField'
import { validate } from "../../util/validators"

// update the state reducer looking for field changes and leaving fields
const inputReducer = (state, action) => {
  switch (action.type) {
    case "CHANGE":
      return {
        ...state,
        value: action.val,
        isValid: validate(action.val, action.validators),
      }
    case "TOUCH": {
      return {
        ...state,
        isTouched: true,
      }
    }
    default:
      return state
  }
}

const Input = (props) => {
  const [inputState, dispatch] = useReducer(inputReducer, {
    value: props.initialValue || "",
    isTouched: false,
    isValid: props.initialValid || false,
  })

  // get just the variables which affect the state
  const { id, onInput } = props
  const { value, isValid } = inputState

  // call the forms on input so its state is also updated
  useEffect(() => {
    onInput(id, value, isValid)
  }, [id, value, isValid, onInput])

  // change the fields state using the reducers dispatch
  // this will invoke the useEffect if the state is changed
  const changeHandler = (event) => {
    dispatch({
      type: "CHANGE",
      val: event.target.value,
      validators: props.validators,
    })
  }
  // to be run when a field is exitted, showing which the user could have changed
  const touchHandler = () => {
    dispatch({
      type: "TOUCH",
    })
  }

  // either input field or text area supported at the moment
  const element =
    props.element === "input" ? (
      <TextField
        {...props.style}
        id={props.id}
        type={props.type}
        placeholder={props.placeholder}
        onChange={changeHandler}
        onBlur={touchHandler}
        value={inputState.value}
      />
    ) : (
      <textarea
        id={props.id}
        rows={props.rows || 3}
        onChange={changeHandler}
        onBlur={touchHandler}
        value={inputState.value}
      />
    )

  
  return (
    <div
      className={`form-control ${
        !inputState.isValid && inputState.isTouched && "form-control--invalid"
      }`}>
      <label htmlFor={props.id}>{props.label}</label>
      {element}
      {!inputState.isValid && inputState.isTouched && <p>{props.errorText}</p>}
    </div>
  )
}

export default Input

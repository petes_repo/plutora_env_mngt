import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useParams, useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { useForm } from "../shared/hooks/form-hook";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  "@global": {
    a: {
      textDecoration: "none",
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function UpdateUser(props) {
  const classes = useStyles();
  const history = useHistory();

  const userId = useParams().userId;
  //const editUser = props.location.user
  //console.log(`editUser:(${editUser})`)
  console.log("the props");
  console.log(props);
  
  // formState stores the state of the fields on the form
  // inputHandler is a callback passed to fields so they can update the form state
  // setFormData loads the state from the get request

  const [formState, inputHandler, setFormData, formHandler] = useForm(
    {
      email: {
        value: "",
        isValid: false,
        errors: [],
      },
      password: {
        value: "",
        isValid: false,
        errors: [],
      },
      role: {
        value: "",
        isValid: false,
        errors: [],
      },
    },
    false,
    []
  );

  // on form load retrieve the user by its id
  useEffect(() => {
    if (props.location.state) {
      try {
        // update the state with the user passed in props
        setFormData(
          {
            email: {
              value: props.location.state.email,
              isValid: true,
              errors: [],
            },
            password: {
              value: "",
              isValid: true,
              errors: [],
            },
            role: {
              value: props.location.state.role,
              isValid: true,
              errors: [],
            },
          },
          true,
          []
        );
      } catch (err) {
        console.log("update-useEffect Error");
        console.log(err);
      }
    }
  }, [props.location.state]);

  // define what happens when the form is submitted
  const onSubmit = async (event) => {
    // stop the default operation
    event.preventDefault();
    // call the update api
    let response
    try {
      if (formState.inputs.password.value === "") {
        response = await axios.put(
          `/api/v1/users/${userId}`,
          {
            email: formState.inputs.email.value,
            role: formState.inputs.role.value
          }
        )
      } else {
        response = await axios.put(
          `/api/v1/users/${userId}`,
          {
            email: formState.inputs.email.value,
            role: formState.inputs.role.value,
            password: formState.inputs.password.value
          }
        )
      }

      // check to see whether there are any errors
      if (response.errors) {
        // initialise the potential errors
        let emailErrors = [];
        let passwordErrors = [];
        let roleErrors = [];
        let formErrors = [];
        // iterate over the errors returned
        response.errors.map((err) => {
          // if there is a field declared then add error to that field
          if (err.field) {
            switch (err.field) {
              case "email":
                emailErrors.push(err.message);
                break;
              case "password":
                passwordErrors.push(err.message);
                break;
              case "role":
                roleErrors.push(err.message);
                break;
              default:
                formErrors.push(err.message);
            }
          } else {
            // otherwise the was no specific field and add to form errors
            formErrors.push(err.message);
          }
        });
        // update the error states
        inputHandler(
          "email",
          formState.inputs.email.value,
          formState.inputs.email.isValid,
          emailErrors
        );
        inputHandler(
          "password",
          formState.inputs.password.value,
          formState.inputs.password.isValid,
          passwordErrors
        );
        inputHandler(
          "role",
          formState.inputs.role.value,
          formState.inputs.role.isValid,
          roleErrors
        );
        formHandler(formErrors);
      } else {
        // request successful so redirect to home page
        history.push("/users");
      }
    } catch (err) {
      console.log("update-onSubmit Error");
      console.log(err);
    }
  };

  // handle a field being updated
  const onChange = async (id, value) => {
    let isValid = false;
    let errors = [];
    // validate the fields where id is the name of the field
    try {
      switch (id) {
        // validate the email field
        case "email":
          isValid = /^\S+@\S+\.\S+$/.test(value);
          if (!isValid) {
            errors.push("Not a valid email");
          }
          break;
        // validate the password
        case "password":
          isValid = value.trim().length >= 6 || value.trim().length === 0;
          if (!isValid) {
            errors.push(
              "Password must be at least 6 characters if you change it"
            );
          }
          break;
        // validate the password
        case "role":
          isValid = value === "user" || value === "admin";
          if (!isValid) {
            errors.push("role must be user or admin");
          }
          break;
        default:
          break;
      }
      // update the field state
      inputHandler(id, value, isValid, errors);
    } catch (err) {
      console.log("update-onChange Error");
      console.log(err);
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <form className={classes.form} onSubmit={onSubmit}>
          <TextField
            required
            fullWidth
            id="email"
            label="Email Address"
            helperText={formState.inputs.email.errors.join("-")}
            error={formState.inputs.email.errors.length !== 0}
            value={formState.inputs.email.value}
            onChange={(event) => onChange("email", event.target.value)}
            autoFocus
          />
          <TextField
            required
            fullWidth
            label="Role"
            type="role"
            id="role"
            helperText={formState.inputs.role.errors.join("-")}
            error={formState.inputs.role.errors.length !== 0}
            value={formState.inputs.role.value}
            onChange={(event) => onChange("role", event.target.value)}
          />
          <TextField
            fullWidth
            label="Password(optional)"
            type="password"
            id="password"
            helperText={formState.inputs.password.errors.join("-")}
            error={formState.inputs.password.errors.length !== 0}
            value={formState.inputs.password.value}
            onChange={(event) => onChange("password", event.target.value)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            disabled={!formState.isValid}
            className={classes.submit}
          >
            Update
          </Button>
        </form>
      </div>
    </Container>
  );
}

import React, { useEffect } from "react"
import useRequest from "../shared/hooks/use-request"
import { useHistory } from "react-router-dom"

// called from main window with the callback to set the apps current user
export default function Signout({ setCurrentUser }) {
  const history = useHistory()

  const { doRequest } = useRequest({
    url: "/api/v1/users/signout",
    method: "post",
    body: {},
    onSuccess: () => {
      setCurrentUser(null)
      history.push("/")
    },
  })

  useEffect(() => {
    doRequest()
  }, [])

  return <div>Signing you out...</div>
}

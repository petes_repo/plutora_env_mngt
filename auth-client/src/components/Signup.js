import React from "react"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import Grid from "@material-ui/core/Grid"
import Box from "@material-ui/core/Box"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"
import { Link, useHistory } from "react-router-dom"

import useRequest from "../shared/hooks/use-request"
import { useForm } from "../shared/hooks/form-hook"

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" to="/">
        Pete's Dev
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  )
}

const useStyles = makeStyles((theme) => ({
  signinpaper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  }
}))

// called from main window with the callback to set the apps current user
export default function SignIn({ setCurrentUser }) {
  const classes = useStyles()
  const history = useHistory()
  // set the form state
  const [formState, inputHandler, setFormData, formHandler] = useForm(
    {
      email: {
        value: "",
        isValid: false,
        errors: [],
      },
      password: {
        value: "",
        isValid: false,
        errors: [],
      },
    },
    false, // whether the form is valid
    [] // initial form errors
  )

  // send the signin request
  const { doRequest, reqErrors } = useRequest({
    url: "/api/v1/users/signup",
    method: "post",
    body: {
      email: formState.inputs.email.value,
      password: formState.inputs.password.value,
    },
    onSuccess: (user) => {
      // update the applications current user state
      setCurrentUser(user)
    },
  })

  // handle the form being submitted
  const onSubmit = async (event) => {
    // stop the default form actions
    event.preventDefault()
    // send the data to the backend
    await doRequest()
    // check to see whether there are any errors
    if (reqErrors) {
      // initialise the errors
      let emailErrors = []
      let passwordErrors = []
      let formErrors = []
      // iterate over the errors returned
      reqErrors.map((err) => {
        // if there is a field declared then add error to that field
        if (err.field) {
          switch (err.field) {
            case "email":
              emailErrors.push(err.message)
              break
            case "password":
              passwordErrors.push(err.message)
              break
            default:
              formErrors.push(err.message)
          }
        } else {
          // otherwise the was no specific field and add to form errors
          formErrors.push(err.message)
        }
      })
      // update the error states
      inputHandler(
        "email",
        formState.inputs.email.value,
        formState.inputs.email.isValid,
        emailErrors
      )
      inputHandler(
        "password",
        formState.inputs.password.value,
        formState.inputs.password.isValid,
        passwordErrors
      )
      formHandler(formErrors)
    } else {
      // request successful so redirect to home page
      history.push("/")
    }
  }

  // handle a field being updated
  const onChange = async (id, value) => {
    let isValid = false
    let errors = []
    // validate the fields where id is the name of the field
    switch (id) {
      // validate the email field
      case "email":
        isValid = /^\S+@\S+\.\S+$/.test(value)
        if (!isValid) {
          errors.push("Not a valid email")
        }
        break
      // validate the password
      case "password":
        isValid = value.trim().length >= 6
        if (!isValid) {
          errors.push("Password must be at least 6 characters")
        }
        break
      default:
        break
    }
    // update the field state
    inputHandler(id, value, isValid, errors)
  }

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.signinpaper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <form onSubmit={onSubmit} className={classes.form} noValidate>
          <TextField
            required
            fullWidth
            id="email"
            label="Email Address"
            helperText={formState.inputs.email.errors.length > 0 && formState.inputs.email.errors.join("-")}
            error={formState.inputs.email.errors.length !== 0}
            value={formState.inputs.email.value}
            onChange={(event) => onChange("email", event.target.value)}
            autoFocus
          />
          <TextField
            required
            fullWidth
            label="Password"
            type="password"
            id="password"
            helperText={formState.inputs.password.errors.length > 0 && formState.inputs.password.errors.join("-")}
            error={formState.inputs.password.errors.length !== 0}
            value={formState.inputs.password.value}
            onChange={(event) => onChange("password", event.target.value)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container>
            <Grid item>
              <Link to="/users/signin">{"Have an account? Sign In"}</Link>
            </Grid>
          </Grid>
          {formState.errors.length > 0 && (
            <div className="alert alert-danger">
              <h4>Ooops....</h4>
              <ul className="my-0">
                {formState.errors.map((err) => (
                  <li key={err.message}>{err.message}</li>
                ))}
              </ul>
            </div>
          )}
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  )
}

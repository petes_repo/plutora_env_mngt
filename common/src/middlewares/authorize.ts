import { Request, Response, NextFunction } from "express"

import { NotAuthorisedError } from "../errors/not-authorised-error"

// Grant access to specific roles
export const authorize = (...roles: any) => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!roles.includes(req.currentUser!.role)) {
      throw new NotAuthorisedError()
    }
    next()
  }
}
